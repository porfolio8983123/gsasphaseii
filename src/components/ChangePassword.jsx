import React, { useRef, useState } from 'react';
import logo from '../img/desung logo 1.png';
import '../style/changePassword/ChangePassword.css';
import {AiFillEyeInvisible} from 'react-icons/ai';
import {AiFillEye} from 'react-icons/ai';
import { useLocation,useNavigate } from 'react-router-dom';

function ChangePassword() {

  const {state} = useLocation();
  const navigate = useNavigate();

  const [password,setPassword] = useState('');
  const [confirmpassword,setconfirmpassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [eyeOpen1,setEyeOpen1] = useState(false);
  const [eyeOpen2,setEyeOpen2] = useState(false);
  const [inputone,setInputOne] = useState(false);
  const [inputtwo,setInputTwo] = useState(false);

  const [error,setError] = useState("");
  
  const [pin, setPin] = useState(['', '', '', '']);

  const handlePinChange = (index, value) => {
    if (/^\d*$/.test(value) && value.length <= 1) {
        const newPin = [...pin];
        newPin[index] = value;
        setPin(newPin);
        
        // Move to the next input if a digit is entered
        if (value && index < 3) {
            document.getElementById(`code${index + 2}`).focus();
        }
    }
  };

  const handlePasswordReset = async (e) => {
    e.preventDefault();
    setIsLoading(true)
    const pinString = pin.join('');
    if (pinString.length !== 4) {
      setError("PinCode is not valid!");
      return;
    }
    
    if (password !== confirmpassword) {
      setError("Password doesn't match!");
      setIsLoading(false);
      return;
    }

    try {
      const response = await fetch('http://localhost:8000/api/resetPassword', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ "email":state,"token":pinString,"password":password })
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        navigate('/');
      } else {
        const data = await response.json();
        setError(data.message)
        setIsLoading(false);
      }

    } catch (error) {
      setError(error.message);
      setIsLoading(false);
    } finally {
      setIsLoading(false);
    }
  }

  console.log(password,confirmpassword);
  console.log(pin)
  console.log("email ",state);

  
  return (
    <div className='changePassword__container'>
      <div className='changePassword__innercontainer'>
      <div className="changePassword__changePassword">
      <img src={logo} alt='logo' style={{ width: 100, height: 100,marginTop:15}} />
      <div className="h4__changePassword">Change Password</div>
      <form className='changePassword__form' onSubmit={handlePasswordReset}>
        {error &&
            <div className="changePassword__error__message">
              <p className=''>{error}</p>
            </div>
          }
        <label className='changePassword__label' htmlFor="password">Enter OTP</label>
       <div className='code__container'>
        {pin.map((digit, index) => (
                  <div key={index} className="changePassword__code__area">
                      <input
                          type="text" // Use type "text" to allow entering a single digit
                          id={`code${index + 1}`}
                          inputMode='numeric'
                          className="changePassword__code__input"
                          maxLength={1}
                          value={digit}
                          onChange={(e) => handlePinChange(index, e.target.value)}
                      />
                  </div>
              ))}
       </div>
        
        <label className='changePassword__label' htmlFor="password">Enter new password</label>
        <div className="changePassword__text__area">
          <input
            type={inputone?"text":"password"}
            id="password"
            name="password"
            className="changePassword__text__input"
            onChange={(e) => setPassword(e.target.value)}
          />
          {eyeOpen1 === true &&
            <AiFillEyeInvisible size={20} className="login__eye__closed" 
              onClick={() => {
                setEyeOpen1(!eyeOpen1)
                setInputOne(!inputone)
              }}
            /> 
          }
          {eyeOpen1 === false &&
            <AiFillEye size={20} className="login__eye__closed" 
              onClick={()=> {
                setEyeOpen1(!eyeOpen1);
                setInputOne(!inputone)
              }}
            />
          } 
        </div>
        
        <label className='changePassword__label' htmlFor="password">Enter confirm password</label>
        <div className="changePassword__text__area">
          <input
            type={inputtwo?"text":"password"}
            id="confirmpassword"
            name="password"
            className="changePassword__text__input"
            onChange={(e) => setconfirmpassword(e.target.value)}
          />
          {eyeOpen2 &&
            <AiFillEyeInvisible size={20} className="login__eye__closed" 
            onClick={() => {
              setEyeOpen2(!eyeOpen2)
              setInputTwo(!inputtwo)
            }}
            /> 
          }
          {!eyeOpen2 &&
            <AiFillEye size={20} className="login__eye__closed" 
              onClick={()=> {
                setEyeOpen2(!eyeOpen2);
                setInputTwo(!inputtwo)
              }}
            />
          }   
        </div>

        <div style={{ textAlign: 'center' }}>

          {isLoading ? (
            <button className="forgotPassword__btn" disabled>
              <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              <span className='ms-2'>Changing...</span>
            </button>
          ):(
            <input
            type="submit"
            value="Continue"
            className="changePassword__btn"
          />
          )
          }
          
        </div>
      </form>
    </div>
    </div>
    </div>
  )
}

export default ChangePassword