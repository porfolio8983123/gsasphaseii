import * as React from 'react';


export default function HSS() {

    return (
        <>
            <div className='container-fluid'>
                <h5 className='text-start ms-4'>HSS Skilling Program (Home Security Service)</h5>
                <div className='border  w-50'>
                    <div className='pt-4 pb-4 ps-4 '>
                        <p className='text-start'>Eligible Criteria:</p>
                        <br></br>
                        <p className='text-start'>Enlistees having minimum of 10 fingers</p>
                    </div>
                </div>
            </div>
        </>
    );
}
