import * as React from 'react';
import { BarChart } from '@mui/x-charts/BarChart';

export default function ComparisonGraph() {
  return (
    <div className='mt-5 pt-3'>
      <div className='mt-1'>
      <BarChart
        series={[
          { data: [35, 44, 24, 34, 45, 56, 67, 23, 34, 5, 67, 12, 13, 14, 15, 16, 17, 18, 19, 20], label: 'Run1' },
          { data: [51, 6, 49, 30, 45, 56, 67, 23, 34, 5, 67, 12, 13, 14, 15, 16, 17, 18, 19, 20], label: 'Run2' },
          { data: [15, 25, 30, 50, 45, 56, 67, 23, 34, 5, 67, 12, 13, 14, 15, 16, 17, 18, 19, 20], label: 'Run3' },
          { data: [60, 50, 15, 25, 45, 56, 67, 23, 34, 5, 67, 12, 13, 14, 15, 16, 17, 18, 19, 20], label: 'Run4' },
        ]}
        height={290}
        xAxis={[
          {
            data: ['Thim', 'Paro', 'Puna', 'Haa', 'Gasa', 'Wang', 'Sarp', 'Tsir', 'Chuk', 'Bumt', 'Mong', 'Tgang', 'Tyang', 'S/Jong', 'Pemag', 'Zhem', 'Samt', 'Daga', 'Lhue', 'Trong'],
            scaleType: 'band',
          }
        ]}
        margin={{ top: 10, bottom: 30, left: 40, right: 10 }}
        slotProps={{
          legend: {
            hidden: true, // Hide the legend
          },
        }}
      />
      </div>
    </div>
  );
}
