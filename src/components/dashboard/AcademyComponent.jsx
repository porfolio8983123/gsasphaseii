import React from 'react'
import ProximityDiversity from '../previewPage/ProximityDiversity';
import Diversity from '../previewPage/Diversity';
import ProximityRange from '../previewPage/ProximityRange';
import TablePreview from '../previewPage/TablePreview';
import { FaArrowTurnDown } from "react-icons/fa6";
import GenderBarGraph from '../previewPage/GenderBarGraph';

const AcademyComponent=({data})=>{
    console.log("from academy component ", data);

    const getGender = (index) => {
        const currentData = data[index];
        console.log("current data ", currentData);

        const genderCounts = currentData?.students.reduce((counts, row) => {
            const gender = row['gender']; 
            counts[gender] = (counts[gender] || 0) + 1;
            return counts;
          }, {});
    
          console.log('Gender Counts:', genderCounts);
        return genderCounts;
    }

    const getTotalStudent = (index) => {
        const currentData = data[index];
        return currentData?.students.length;
    }

    return(
        <div className=''>
            {data && data.length > 0 &&
                data.map((item,index) => (
                    <div className=' shadow-lg p-3 mb-5 bg-white rounded'>
                        <h4 className='mt-4 ms-3' style={{color:"#F04A00"}}>{item.name}</h4>
                            <div className='mt-2  d-flex justify-content-between'>
                                <div className=' mt-2 ms-5'>
                                    <h6 className='' style={{color:'black',opacity:'60%',fontWeight:'bold'}} >Total Capacity</h6>
                                    <p style={{color:"#F04A00",fontWeight:'bold'}}>{getTotalStudent(index)} / {item.numberOfFemale + item.numberOfMale}</p>
                                </div>
                                <div className=' mt-2 me-5'>
                                    <h6 className='' style={{color:'black',opacity:'60%',fontWeight:'bold'}} >Total Male Intake</h6>
                                    <p style={{color:"#F04A00",fontWeight:'bold'}}>{getGender(index).M}</p>
                                </div>
                                <div className=' mt-2 me-5'>
                                    <h6 className='' style={{color:'black',opacity:'60%',fontWeight:'bold'}} >Total Female Intake</h6>
                                    <p style={{color:"#F04A00",fontWeight:'bold'}}>{getGender(index).F}</p>
                                </div>
                            </div>

                            <div className='d-flex justify-content-between '>
                                <ProximityDiversity capacity = {item.capacity} proximity = {item.proximity} type = "actual" students = {item.students}/>
                                <ProximityRange proximityRange = {item.proximityRange}/>
                                <Diversity east = {item.east} west = {item.west} south = {item.south} central = {item.central}/>
                                {/* <GenderBarGraph numberOfMale = {item.numberOfMale} numberOfFemale = {item.numberOfFemale} students = {item.students} type = "actual"/> */}
                            </div>
                        <div className='' style={{borderRight:"none"}}>
                            <TablePreview students={item.students} type="dashboard"/>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default AcademyComponent;