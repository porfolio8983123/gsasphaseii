import React, { useEffect, useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { setPemathangCentralMale, setPemathangNumberOfFemale,setPemathangNumberOfMale, setPemathangProximityMale, setPemathangSouthMale, setPemathangWestMale } from '../../../store';
import { useFetchAcademiesQuery } from '../../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SettingGender = ({settingPemathangGenderError}) => {

    const dispatch = useDispatch();

    const pemathangData = useSelector((state) => state.pemathang);

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [pemathangCapacity,setPemathangCapacity] = useState(0);
  
    useEffect(() => {
      if (data) {
        // Check if data.academies is defined before accessing it
        if (data.academies) {
          data.academies.forEach(element => {
            if (element.name === "Pemathang Academy") {
              setPemathangCapacity(element.capacity);
              return;
            }
          });
        }
      }
    }, [data]);

    console.log("from gender ",pemathangData);

    const [maleCount, setMaleCount] = useState(0);
    const [femaleCount, setFemaleCount] = useState(0);
    const [southMaleCount,setSouthMaleCount] = useState(0);
    const [proximityMaleCount,setProximityMaleCount] = useState(0);
    const [westMaleCount,setWestMaleCount] = useState(0);
    const [centralMaleCount,setCentralMaleCount] = useState(0);

    const [maleExceed,setMaleExceed] = useState(false);

    useEffect(() => {
        let total = maleCount + femaleCount;
        if (total > pemathangCapacity || total < pemathangCapacity) {
            settingPemathangGenderError(true);
            setMaleExceed(true);
            toast.error('Total student exceeding or lower the capacity!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        } else {
            setMaleExceed(false);
            settingPemathangGenderError(false);
        }
    },[maleCount,femaleCount])

    useEffect(() => {
        setMaleCount(pemathangData.numberOfMale)
        setFemaleCount(pemathangData.numberOfFemale)
        setSouthMaleCount(pemathangData.southMale)
        setWestMaleCount(pemathangData.westMale)
        setProximityMaleCount(pemathangData.eastMale)
        setCentralMaleCount(pemathangData.centralMale)
    },[pemathangData])

    const handleMaleCountChange = (e) => {
        setMaleCount(parseInt(e.target.value));
        dispatch(setPemathangNumberOfMale(parseInt(e.target.value)));
    };

    const handleFemaleCountChange = (e) => {
        setFemaleCount(parseInt(e.target.value));
        dispatch(setPemathangNumberOfFemale(parseInt(e.target.value)));
    };

    const handleSouthMaleChange = (e) => {
        setSouthMaleCount(parseInt(e.target.value,10));
        dispatch(setPemathangSouthMale(parseInt(e.target.value)));
    }

    const handleProximityMaleChange = (e) => {
        setProximityMaleCount(parseInt(e.target.value,10));
        dispatch(setPemathangProximityMale(parseInt(e.target.value)));
    }

    const handleWestMaleChange = (e) => {
        setWestMaleCount(parseInt(e.target.value,10));
        dispatch(setPemathangWestMale(parseInt(e.target.value)));
    }

    const handleCentralMaleChange = (e) => {
        setCentralMaleCount(parseInt(e.target.value,10));
        dispatch(setPemathangCentralMale(parseInt(e.target.value)));
    }

    // useEffect(() => {
    //     const total = westMaleCount + proximityMaleCount + centralMaleCount + southMaleCount;

    //     console.log("total male from diversity and proximity ",total);
        
    //     if (parseInt(total) > maleCount) {
    //         setMaleExceed(true);
    //         toast.error('Total male exceeding!', {
    //             position: "top-right",
    //             autoClose: 5000,
    //             hideProgressBar: false,
    //             closeOnClick: true,
    //             pauseOnHover: true,
    //             draggable: true,
    //             progress: undefined,
    //             theme: "light",
    //         });
    //     } else {
    //         setMaleExceed(false);
    //     }

    // },[westMaleCount,southMaleCount,proximityMaleCount,centralMaleCount])


    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <h6>Capacity: {pemathangCapacity?pemathangCapacity:0}</h6>
            <form action="" className="mt-4">
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                {/* <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Proximity Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={proximityMaleCount}
                        onChange={handleProximityMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">South Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={southMaleCount}
                        onChange={handleSouthMaleChange}
                    />
                </div>
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">West Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={westMaleCount}
                        onChange={handleWestMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Central Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={centralMaleCount}
                        onChange={handleCentralMaleChange}
                    />
                </div> */}
            </form>
        </div>
    );
};

export default SettingGender;