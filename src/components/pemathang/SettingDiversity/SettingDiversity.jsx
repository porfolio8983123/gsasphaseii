import React, { useState } from 'react';
import { PieChart, Pie, Cell } from 'recharts';
import './SettingDiversity.css';
import { useDispatch,useSelector } from 'react-redux';
import { setPemathangCentral,setPemathangSouth,setPemathangWest } from '../../../store';
import { useEffect } from 'react';
import { useFetchAcademiesQuery } from '../../../store';

const COLORS = ['#02344F', '#EEB200', '#F04A00'];

const SettingDiversity = () => {

    const dispatch = useDispatch();
    const [proximity,setProximity] = useState();
    const [percentage,setPercentage] = useState();
    const [proximityVariation,setProximityVariation] = useState(false);

    const pemathangData = useSelector((state)=> state.pemathang)

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [pemathangCapacity,setPemathangCapacity] = useState(0);
  
    useEffect(() => {
      if (data) {
        // Check if data.academies is defined before accessing it
        if (data.academies) {
          data.academies.forEach(element => {
            if (element.name === "Pemathang Academy") {
              setPemathangCapacity(element.capacity);
              return;
            }
          });
        }
      }
    }, [data]);

    console.log("from diversity ", pemathangData);

    const [sliderValues, setSliderValues] = useState({
        South: 0,
        Central: 0,
        West: 0,
    });

    useEffect(() => {
        setSliderValues({
            South:pemathangData.south, 
            Central:pemathangData.central, 
            West: pemathangData.west
        })
    },[pemathangData,data])

    useEffect(() => {
        setProximity(pemathangData.proximity)
        if (proximity) {
            setPercentage(pemathangCapacity-proximity);
        }

        if (percentage < sliderValues.South || percentage < sliderValues.Central ||percentage < sliderValues.West) {
            setProximityVariation(true);
        }
    },[pemathangData,percentage,proximity,data])

    useEffect(() => {
        if (proximityVariation === true) {
            setSliderValues({
                South: 0,
                Central: 0,
                West: 0,
            })
    
            dispatch(setPemathangCentral(0))
            dispatch(setPemathangWest(0))
            dispatch(setPemathangSouth(0));
        }
    },[proximityVariation])

    console.log("Promity ",proximity);

    const handleSliderChange = (name, value) => {
        console.log("handling change slider ",value);

        const total = sliderValues.South + sliderValues.Central + sliderValues.West - sliderValues[name];

        // Distribute the remaining value evenly to other sliders
        if (total + value <= percentage) {
            const updatedValues = {
                South: name === 'South' ? value : sliderValues.South,
                Central: name === 'Central' ? value : sliderValues.Central,
                West: name === 'West' ? value : sliderValues.West,
            };
    
            setSliderValues(updatedValues);
            dispatch(setPemathangCentral(parseInt(updatedValues.Central)))
            dispatch(setPemathangWest(parseInt(updatedValues.West)))
            dispatch(setPemathangSouth(parseInt(updatedValues.South)));
        }
    };

    const pdata = [
        { name: 'South', value: sliderValues.South, fill: COLORS[0] },
        { name: 'Central', value: sliderValues.Central, fill: COLORS[1] },
        { name: 'West', value: sliderValues.West, fill: COLORS[2] },
    ];

    return (
        <div className={`${pemathangData.proximity === pemathangCapacity ?'d-none':''}`}>
            <h6 className='ms-5 ps-5'>Diversity Intake Percentage</h6>
            <div className='d-flex justify-content-evenly align-item-center' style={{marginTop:"-5%"}}>
            
                <PieChart width={400} height={400}> 
                    <Pie 
                        data={pdata}
                        dataKey="value"
                        nameKey="name"
                        outerRadius={80}
                        fill="#8884d8"
                        labelLine={false}
                        label={({
                            cx,
                            cy,
                            midAngle,
                            innerRadius,
                            outerRadius,
                            percent,
                            index,
                        }) => {
                            const RADIAN = Math.PI / 180;
                            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
                            const x = cx + radius * Math.cos(-midAngle * RADIAN);
                            const y = cy + radius * Math.sin(-midAngle * RADIAN);

                            return (
                                <text
                                    x={x}
                                    y={y}
                                    fill="#FFFFFF"
                                    textAnchor="middle"
                                    dominantBaseline="middle"
                                >
                                    {`${pdata[index].name}`}
                                </text>
                            );
                        }}
                    >
                        {pdata.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
                <div className='d-flex flex-column align-item-center justify-content-center me-5 px-5'style={{width:"35%"}}>
                    {proximity? (
                        <h4>East {proximity}</h4>
                    ):(
                        <h4>East 0</h4>
                    )}
                    <div className='d-flex '>
                        <label>{parseInt(sliderValues.South)}</label>
                        <input 
                            className='w-100 slider1 mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            value={sliderValues.South}
                            onChange={(e) => {
                                console.log("south data ",e.target.value)
                                handleSliderChange('South', parseInt(e.target.value, 10))
                            }}
                            style={{ background: COLORS[0] }} // Set background color to #02344F
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                    <div className='d-flex mt-2  w-100'>
                        <label>{parseInt(sliderValues.Central)}</label>
                        <input
                            className='w-100 slider2 mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            value={sliderValues.Central}
                            onChange={(e) => handleSliderChange('Central', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[1], backgroundColor: '#EEB200' }} // Set background color to #EEB200
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                    <div className='d-flex mt-2'>
                        <label>{parseInt(sliderValues.West)}</label>
                        <input
                            className='w-100 slider3  mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            
                            value={sliderValues.West}
                            onChange={(e) => handleSliderChange('West', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[2] }} // Set background color to #F04A00
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SettingDiversity;
