import React from "react";
import SettingGender from "./SettingGender/SettingGender";
import SettingProximityDiversity from './SettingProximity/SettingProximityDiversity';
import SettingRangeProximity from './SettingProximityRange/SettingRangeProximity';
// import SettingDiversity from './SettingDiversity/SettingDiversity';

const Pemathang=({settingPemathangGenderError})=>{
    return(
        <div className="p-3">
            <div className="">

                <div className="d-flex justify-content-between mt-5">
                    <SettingProximityDiversity/>
                    <SettingRangeProximity/>
                    <SettingGender settingPemathangGenderError = {settingPemathangGenderError} />
                </div>
                {/* <hr />
                <SettingDiversity/> */}
            </div>
        </div>
    )
}
export default Pemathang;