import React, { useEffect, useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { setJamtsholingNumberOfFemale,setJamtsholingNumberOfMale,setJamtsholingCentralMale,setJamtsholingSouthMale,setJamtsholingEastMale,setJamtsholingProximityMale, setProximityMale } from '../../../store';
import { useFetchAcademiesQuery } from '../../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SettingGender = ({settingJamtsholingGenderError}) => {

    const dispatch = useDispatch();

    const jamtsholingData = useSelector((state) => state.jamtsholing);

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [jamtsholingCapacity,setJamtsholingCapacity] = useState(0);

    useEffect(() => {
        if (data) {
        // Check if data.academies is defined before accessing it
        if (data.academies) {
            data.academies.forEach(element => {
            if (element.name === "Jamtsholing Academy") {
                setJamtsholingCapacity(element.capacity);
                return;
            }
            });
        }
        }
    }, [data]);

    console.log("from gender ",jamtsholingData);

    const [maleCount, setMaleCount] = useState(0);
    const [femaleCount, setFemaleCount] = useState(0);
    const [southMaleCount,setSouthMaleCount] = useState(0);
    const [proximityMaleCount,setProximityMaleCount] = useState(0);
    const [eastMaleCount,setEastMaleCount] = useState(0);
    const [centralMaleCount,setCentralMaleCount] = useState(0);

    const [maleExceed,setMaleExceed] = useState(false);

    useEffect(() => {
        let total = maleCount + femaleCount;
        if (total > jamtsholingCapacity || total < jamtsholingCapacity) {
            settingJamtsholingGenderError(true);
            setMaleExceed(true);
            toast.error('Total student exceeding or lower the capacity!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        } else {
            setMaleExceed(false);
            settingJamtsholingGenderError(false);
        }
    },[maleCount,femaleCount])

    useEffect(() => {
        setMaleCount(jamtsholingData.numberOfMale)
        setFemaleCount(jamtsholingData.numberOfFemale)
        setCentralMaleCount(jamtsholingData.centralMale)
        setProximityMaleCount(jamtsholingData.westMale);
        setSouthMaleCount(jamtsholingData.southMale)
        setEastMaleCount(jamtsholingData.eastMale)
    },[jamtsholingData])

    const handleMaleCountChange = (e) => {
        setMaleCount(parseInt(e.target.value));
        dispatch(setJamtsholingNumberOfMale(parseInt(e.target.value)));
    };

    const handleFemaleCountChange = (e) => {
        setFemaleCount(parseInt(e.target.value));
        dispatch(setJamtsholingNumberOfFemale(parseInt(e.target.value)));
    };

    const handleSouthMaleChange = (e) => {
        setSouthMaleCount(parseInt(e.target.value,10));
        dispatch(setJamtsholingSouthMale(parseInt(e.target.value)));
    }

    const handleProximityMaleChange = (e) => {
        setProximityMaleCount(parseInt(e.target.value,10));
        dispatch(setJamtsholingProximityMale(parseInt(e.target.value)));
    }

    const handleEastMaleChange = (e) => {
        setEastMaleCount(parseInt(e.target.value,10));
        dispatch(setJamtsholingEastMale(parseInt(e.target.value)));
    }

    const handleCentralMaleChange = (e) => {
        setCentralMaleCount(parseInt(e.target.value,10));
        dispatch(setJamtsholingCentralMale(parseInt(e.target.value)));
    }

    // useEffect(() => {
    //     const total = eastMaleCount + proximityMaleCount + centralMaleCount + southMaleCount;

    //     console.log("total male from diversity and proximity ",total);
        
    //     if (parseInt(total) > maleCount) {
    //         setMaleExceed(true);
    //         toast.error('Total male exceeding!', {
    //             position: "top-right",
    //             autoClose: 5000,
    //             hideProgressBar: false,
    //             closeOnClick: true,
    //             pauseOnHover: true,
    //             draggable: true,
    //             progress: undefined,
    //             theme: "light",
    //         });
    //     } else {
    //         setMaleExceed(false);
    //     }

    // },[eastMaleCount,southMaleCount,proximityMaleCount,centralMaleCount])


    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <h6>Capacity: {jamtsholingCapacity?jamtsholingCapacity:0}</h6>
            <form action="" className="mt-4">
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                {/* <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Proximity Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={proximityMaleCount}
                        onChange={handleProximityMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">South Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={southMaleCount}
                        onChange={handleSouthMaleChange}
                    />
                </div>
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">East Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={eastMaleCount}
                        onChange={handleEastMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Central Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={centralMaleCount}
                        onChange={handleCentralMaleChange}
                    />
                </div> */}
            </form>
        </div>
    );
};

export default SettingGender;