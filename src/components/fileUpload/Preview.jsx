import React, { useEffect, useState } from 'react';
import Papa from 'papaparse';
import ReactPaginate from 'react-paginate';
import graduateCap from '../../img/game-icons_graduate-cap.png';
import Cookies from 'js-cookie';
import {GrNext,GrPrevious} from 'react-icons/gr';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Select from "react-select";
import FileUploadPreview from './FileUploadPreview';
import { useFetchAcademiesQuery } from "../../store";
import PreviewTable from '../tables/PreviewTable';

function Preview({ setPreviousStep, fileName, selectedFile, setCompleteStep,setActiveStep }) {

  const [token,setToken] = useState('');
  const [isFileSaving,setIsFileSaving] = useState(false);
  const [totalMale,setTotalMale] = useState(0);
  const [totalFemale,setTotalFemale] = useState(0);
  const [genderValue, setGenderValue] = useState("All");
  const [cidError,setCIDError] = useState(false);

  const [searchCid, setSearchCid] = useState('');
  const [searchGender, setSearchGender] = useState('');
  const [filteredData, setFilteredData] = useState([]);

  const genderOptions = [
    {value: "All", label: "All"},
    { value: "M", label: "Male" },
    { value: "F", label: "Female" },
  ];

  const itemsPerPage = 10;
  const [currentPage, setCurrentPage] = useState(0);
  const [csvData, setCsvData] = useState(null);
  const [capacity,setCapacity] = useState(0);
  const [studentName,setStudentName] = useState('');

  const {data,isLoading,isError} = useFetchAcademiesQuery();

  console.log("from preview to get academy ", data);

  useEffect(() => {
      let capacity = 0;
      if (data) {
          data?.academies.map((item) => {
              console.log(item.capacity);
              capacity += item.capacity;
          })
      }
      setCapacity(capacity);
  },[data])

  useEffect(() => {
    if (selectedFile) {
      console.log("selected file ", selectedFile);
      Papa.parse(selectedFile, {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        complete: (result) => {
          setCsvData(result.data);
        },
        error: (error) => {
          console.error('Error parsing CSV ', error);
        },
      });
    }
  }, [selectedFile]);

  useEffect(() => {
    const token = Cookies.get('auth_token');
    setToken(token);
  },[])

  useEffect(() => {
    if (csvData) {
      for (const row of csvData) {
        if ('CID' in row) {
          const cid = row['CID'];
  
          if (cid && (!(cid && cid.toString().length === 11 && /^\d+$/.test(cid)))) {
            setCIDError(true);
            break;
          }
        }
      }
    }
  }, [csvData]);

  useEffect(() => {
    if (csvData) {
      const genderCounts = csvData.reduce((counts, row) => {
        const gender = row['SEX']; 
        const cid = row['CID'];
        counts[gender] = (counts[gender] || 0) + 1;

        if (cid && cid.length > 11 || cid.length < 11) {
          setCIDError(true);
        }

        return counts;
      }, {});

      console.log('Gender Counts:', genderCounts);

      setTotalMale(genderCounts.M);
      setTotalFemale(genderCounts.F);
    }

    console.log(totalMale,totalFemale);
  }, [csvData]);

  const handlePageChange = ({ selected }) => {
    setCurrentPage(selected);
  };

  const startIndex = currentPage * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentData = csvData && csvData.slice(startIndex, endIndex);

  const handleComplete = async () => {

    if (csvData.length > capacity) {
      toast.error(`Your student exceeds the capacity!`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    if (cidError) {
      toast.error(`CID does not have a length of 11.`, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      return;
    }

    setIsFileSaving(true);
    const formData = new FormData();
    formData.append('file',selectedFile);
    formData.append('fileName',fileName);

    await fetch('http://localhost:8000/api/fileUpload',{
      method:'POST',
      headers: {
        'Authorization':`Bearer ${token}`
      },
      body:formData,
    })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      if (data.status === 'success') {
        toast.success("File uploaded successfully!", {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });

        setIsFileSaving(false)
        setTimeout(() => {
          setActiveStep(0)
        },3000)
      }
    })
    .catch((error) => {
      console.log("eror ",error);
      toast.error("Something went wrong!", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });

      setIsFileSaving(false)
    })
  };

  return (
    <div className="d-flex" style={{width:'70%'}}>
      <div className='' style={{marginTop:'30px',width:'100%'}}>
        <div className="left-panel" style={{ borderRight: '1px solid #ccc', padding: '20px',maxHeight: 'calc(100vh - 130px)', overflowY: 'auto'}}>
          <div className="shadow p-3 bg-body rounded">
            {/* {csvData?.length > 0 &&
              <FileUploadPreview students={csvData} />
            } */}

            {csvData?.length > 0 && 
              <PreviewTable students = {csvData}/>
            }
          </div>
          <div className='d-flex justify-content-between mt-5' style={{marginBottom:100}}>
            <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
            <div>
              {isFileSaving ? (
                <button type="button" className="btn" style={{backgroundColor:'#F04A00',color:"white",paddingLeft:'50px',paddingRight:'50px'}} disabled>
                  <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              </button>
              ):(
                <button className="btn" onClick={handleComplete} style={{ backgroundColor: '#F04A00', color: 'white' }}>
                  Confirm
                </button>
              )

              }
            </div>
          </div>
        </div>
      </div>
      <div className="right-panel" style={{ padding: '10px',marginRight:'-20px',marginTop:'35px' }}>
        <div action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px',color:'white' }}>
          <p className='text-center'><img src={graduateCap} alt='graduate cap' style={{width:'30px',height:'30px'}}/></p>
          <h5 className='text-center text-light'>Enlistee Information</h5>
          <div className='mt-4 text-center'>
            <h6>{fileName}</h6>
            <div className='d-flex justify-content-center' style={{marginTop:'-15px'}}>
              <hr style={{width:'100px'}}/>
            </div>
            <h6>Total Enlistee: <span style={{color:"#F04A00"}}>{csvData ? csvData.length : 0}</span></h6>
            <h6>Total Male: <span style={{color:"#F04A00"}}>{totalMale}</span></h6>
            <h6>Total Female: <span style={{color:"#F04A00"}}>{totalFemale}</span></h6>
          </div>
        </div>
      </div>
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />
    </div>
    
  );
}

export default Preview;
