import React, { useState, useEffect } from 'react';
import './floating.css';
import { useSelector } from 'react-redux';
import { useFetchAcademiesQuery } from '../../store';

function FloatingComponent() {

  const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

  const [isExpand,setIsExpand] = useState(false);

  const [position, setPosition] = useState({ x:window.innerWidth - 1280, y: 2 });
  const [isDragging, setIsDragging] = useState(false);
  const [unallocatedStudentMale,setUnallocatedStudentMale] = useState(0);
  const [unallocatedStudentFemale,setUnallocatedStudentFemale] = useState(0);
  const [allocatedEast,setAllocatedEast] = useState(0);
  const [allocatedWest,setAllocatedWest] = useState(0);
  const [allocatedCentral,setAllocatedCentral] = useState(0);
  const [allocatedSouth,setAllocatedSouth] = useState(0);

  const [totalMaleEast,setTotalMaleEast] = useState(0);
  const [totalMaleWest,setTotalMaleWest] = useState(0);
  const [totalMaleSouth,setTotalMaleSouth] = useState(0);
  const [totalMaleCentral,setTotalMaleCentral] = useState(0)

  const [gyalpozhingCapacity,setGyalpozhingCapacity] = useState(0);
  const [pemathangCapacity,setPemathangCapacity] = useState(0);
  const [jamtsholingCapacity,setJamtsholingCapacity] = useState(0);
  const [khotokhaCapacity,setKhotokhaCapacity] = useState(0);

  const gyalpozhingData = useSelector((state) => state.gyalpozhing);
  const pemathangData = useSelector((state) => state.pemathang);
  const jamtsholingData = useSelector((state) => state.jamtsholing);
  const khotokhaData = useSelector((state) => state.khotokha);
  const studentCountDetails = useSelector((state) => state.studentCount);

  console.log(studentCountDetails);

  useEffect(() => {
    if (data) {
      // Check if data.academies is defined before accessing it
      if (data.academies) {
        data.academies.forEach(element => {
          if (element.name === "Gyalpozhing Academy") {
            setGyalpozhingCapacity(element.capacity);
          } else if (element.name === "Pemathang Academy") {
            setPemathangCapacity(element.capacity);
          } else if (element.name === "Jamtsholing Academy") {
            setJamtsholingCapacity(element.capacity);
          } else {
            setKhotokhaCapacity(element.capacity);
          }
        });
      }
    }
  }, [data]);

  console.log("Gyalpozhing capacity", gyalpozhingCapacity,pemathangCapacity,jamtsholingCapacity,khotokhaCapacity)

  const getEastChange = () => {
    const fromGyalpozhing = gyalpozhingData.proximity;
    const fromPemathang = pemathangData.proximity;
    const fromKhotokha = khotokhaData.east;
    const fromjamtsholing =jamtsholingData.east;

    const totalEast = fromGyalpozhing + fromPemathang + fromKhotokha +fromjamtsholing;

    setAllocatedEast(totalEast)
    console.log(allocatedEast + fromPemathang)
  }

  const getCentralChange = () => {
    const fromGyalpozhing = gyalpozhingData.central;
    const fromPemathang = pemathangData.central;
    const fromKhotokha = khotokhaData.proximity;
    const fromjamtsholing = jamtsholingData.central;

    const totalCentral = fromGyalpozhing + fromPemathang + fromKhotokha +fromjamtsholing;

    setAllocatedCentral(totalCentral)
  }

  const getWestChange = () => {
    const fromGyalpozhing = gyalpozhingData.west;
    const fromPemathang = pemathangData.west;
    const fromKhotokha = khotokhaData.west;
    const fromjamtsholing = jamtsholingData.proximity;

    const totalWest = fromGyalpozhing + fromPemathang + fromKhotokha +fromjamtsholing;

    setAllocatedWest(totalWest)
  }

  const getSouthChange = () => {
    const fromGyalpozhing = gyalpozhingData.south;
    const fromPemathang = pemathangData.south;
    const fromKhotokha = khotokhaData.south;
    const fromjamtsholing = jamtsholingData.south;

    const totalSouth = fromGyalpozhing + fromPemathang + fromKhotokha +fromjamtsholing;

    setAllocatedSouth(totalSouth);
  }

  const getTotalMaleFromEast = () => {
    const total = gyalpozhingData.eastMale + pemathangData.eastMale + jamtsholingData.eastMale + khotokhaData.eastMale;
    setTotalMaleEast(total)
  }

  const getTotalMaleFromWest = () => {
    const total = gyalpozhingData.westMale + pemathangData.westMale + jamtsholingData.westMale + khotokhaData.westMale;
    console.log("west ", total)
    setTotalMaleWest(total)
  }

  const getTotalMaleFromSouth = () => {
    const total = gyalpozhingData.southMale + pemathangData.southMale + jamtsholingData.southMale + khotokhaData.southMale;
    console.log("south ", total)
    setTotalMaleSouth(total)
  }

  const getTotalMaleFromCentral = () => {
    const total = gyalpozhingData.centralMale + pemathangData.centralMale + jamtsholingData.centralMale + khotokhaData.centralMale;
    console.log("central ", total)
    setTotalMaleCentral(total)
  }

  useEffect(() => {

    const totalMale = studentCountDetails.totalMale;
    const totalFemale = studentCountDetails.totalFemale;

    const allocatedMale = gyalpozhingData.numberOfMale + pemathangData.numberOfMale + jamtsholingData.numberOfMale + khotokhaData.numberOfMale;
    const allocatedFemale = gyalpozhingData.numberOfFemale + pemathangData.numberOfFemale + jamtsholingData.numberOfFemale + khotokhaData.numberOfFemale;
    
    
    console.log(khotokhaData);


    console.log(totalFemale,allocatedFemale);

    setUnallocatedStudentMale(totalMale - allocatedMale);
    setUnallocatedStudentFemale(totalFemale - allocatedFemale);

    getEastChange();
    getWestChange();
    getCentralChange();
    getSouthChange();
    getTotalMaleFromCentral();
    getTotalMaleFromWest();
    getTotalMaleFromEast();
    getTotalMaleFromSouth();

  },[gyalpozhingData,pemathangData,jamtsholingData,khotokhaData,studentCountDetails])



  useEffect(() => {
    if (isDragging) {
      const onMouseMove = (e) => {
        setPosition({
          x: position.x + e.movementX,
          y: position.y + e.movementY,
        });
      };

      const onMouseUp = () => {
        setIsDragging(false);
        window.removeEventListener('mousemove', onMouseMove);
        window.removeEventListener('mouseup', onMouseUp);
      };

      window.addEventListener('mousemove', onMouseMove);
      window.addEventListener('mouseup', onMouseUp);
    }
  }, [isDragging, position]);

  const handleMouseDown = () => {
    setIsDragging(true);
  };

  return (
    <div
      className="floating-component"
      style={{ top: position.y, left: position.x,width:'350px'}}
      onMouseDown={handleMouseDown}
    >
      <div className='py-2 d-flex justify-content-between align-items-center' style={{backgroundColor:'gray'}}>
        <h6 className='ms-4' style={{color:'white'}}>Reference</h6>
        {isExpand?(
          <button className='btn me-4' style={{backgroundColor:'#F04A00',color:"white"}}
            onClick={() => setIsExpand(false)}
          >Shrink</button>
        ):(
          <button className='btn me-4' style={{backgroundColor:'#F04A00',color:"white"}}
            onClick={() => setIsExpand(true)}
          >Expand</button>
        )

        }
      </div>
      <div className='p-4' style={{display: isExpand?'':'none'}}>
        <p style={{fontWeight:'bold'}}>Student Details</p>

        <div>
         <div className='d-flex justify-content-between align-items-center'>
          <div>
          <p style={{fontSize:'12px'}}>Total Student: <span style={{color:"#F04A00"}}>{(studentCountDetails?.totalMale + studentCountDetails?.totalFemale)}</span></p>

          <p style={{fontSize:'12px'}}>Total Male: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalMale}</span></p>

          <p style={{fontSize:'12px'}}>Total Female: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalFemale}</span></p>

          </div>
         </div>

          <p style={{fontWeight:'bold'}}>Student From: </p>

          <div className='d-flex justify-content-between align-items-center'>
            <div>
              <p style={{fontSize:'12px'}}>East: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalEast}</span></p>

              <p style={{fontSize:'12px'}}>West: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalWest}</span></p>

              <p style={{fontSize:'12px'}}>South: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalSouth}</span></p>


              <p style={{fontSize:'12px'}}>Central: <span style={{color:"#F04A00"}}>{studentCountDetails?.totalCentral}</span></p>

            </div>
          </div>

        </div>
        
      </div>
    </div>
  );
}

export default FloatingComponent;
