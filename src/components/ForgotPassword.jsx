import React, { useRef, useState } from 'react';
import logo from '../img/desung logo 1.png';
import '../style/forgotPassword/ForgotPassword.css'
import { useNavigate } from 'react-router-dom';

function ForgotPassword() {
    const [email,setEmail] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const [error,setError] = useState('');

    const handleEmailSending = async (e) => {
      e.preventDefault();
      setIsLoading(true);
      try {
        const response = await fetch('http://localhost:8000/api/sendEmail', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email })
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data);
        navigate('/changepassword',{state:email});
      } else {
        const data = await response.json();
        setError(data.message)
        setIsLoading(false);
      }
      } catch (error) {
        console.log(error.message);
        setIsLoading(false);
      } finally {
        setIsLoading(false);
      }
    }

    console.log(email)

return (
    <div className='forgotPassword__container'>
      <div className='forgotPassword__innercontainer'>
      <div className="forgotPassword__forgotPassword">
      <img src={logo} alt='logo' style={{ width: 100, height: 100,marginTop:15}} />
      <div className="h4__forgotPassword">Forgot Your Password?</div>
      <form className='forgotPassword__form' onSubmit={handleEmailSending}>
        {error &&
            <div className="forgotpassword__error__message">
              <p className=''>{error}</p>
            </div>
          }
        <label className='forgotPassword__label' htmlFor="Email">Enter email address</label>
        <div className="forgotPassword__text__area">
          <input
            type="email"
            id="Email"
            name="Email"
            className="forgotPassword__text__input"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        
        <div style={{ textAlign: 'center' }}>
          {isLoading ? (
                <button className="forgotPassword__btn" disabled>
                  <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                  <span className='ms-2'>Sending...</span>
                </button>
              ) : (
                <input type="submit" value="Submit" className="forgotPassword__btn" />
            )}
        </div>
      </form>
    </div>
    </div>
    </div>
  )
}

export default ForgotPassword;