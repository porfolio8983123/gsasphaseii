import React from 'react'
import GenderBarGraph from './GenderBarGraph';
import ProximityDiversity from './ProximityDiversity';
import ProximityRange from './ProximityRange';
import Diversity from './Diversity';
import TablePreview from './TablePreview';
import { CSVLink } from 'react-csv';
import { useState } from 'react';
import { useEffect } from 'react';
import { FaArrowTurnDown } from "react-icons/fa6";
import {Tooltip} from 'react-tooltip';

const Main=({data})=>{
    const [allStudent,setAllStudent] = useState([]);

    useEffect(() => {
      if (data) {
        let allData = [];

        let gd = [];
    
        data?.academyStudents[0].students.forEach(element => {
          let object = {
            CID: element.CID,
            name:element.name,
            gender: element.gender,
            dzongkhag:element.dzongkhag,
            gewog:element.gewog,
            academy:data.academyStudents[0].name,
            zone:element.zone
          }
    
          gd.push(object);
        });

        allData.push(gd);

        let pd = [];
    
        data?.academyStudents[1].students.forEach(element => {
          let object = {
            CID: element.CID,
            name:element.name,
            gender: element.gender,
            dzongkhag:element.dzongkhag,
            gewog:element.gewog,
            academy:data.academyStudents[1].name,
            zone:element.zone
          }
    
          pd.push(object);
        });

        allData.push(pd);

        let jd = [];
    
        data?.academyStudents[2].students.forEach(element => {
          let object = {
            CID: element.CID,
            name:element.name,
            gender: element.gender,
            dzongkhag:element.dzongkhag,
            gewog:element.gewog,
            academy:data.academyStudents[2].name,
            zone:element.zone
          }
    
          jd.push(object);
        });

        allData.push(jd);

        let kd = [];
    
        data?.academyStudents[3].students.forEach(element => {
          let object = {
            CID: element.CID,
            name:element.name,
            gender: element.gender,
            dzongkhag:element.dzongkhag,
            gewog:element.gewog,
            academy:data.academyStudents[3].name,
            zone:element.zone
          }
    
          kd.push(object);
        });

        allData.push(kd);
    
        setAllStudent(allData);
      }
        
      },[data])

    console.log("from main all student ", data);

    return(
        <div className='mb-5'>
            {data?.academyStudents && data?.academyStudents.length > 0 &&
                data?.academyStudents.map((item,index) => (
                    <div key={index} className='mb-5'style={{width:"99%",}}>
                        <div className=' shadow-lg  mb-5 bg-white rounded'>
                            <div className='d-flex justify-content-between align-items-center'>
                                <h4 className=' ms-3 pt-4'>{item.name}</h4>
                                <div className='pt-4' style={{marginRight:'35px'}}>
                                {allStudent[index] && allStudent[index].length > 0 ? (
                                  <>
                                    <CSVLink
                                      data-tooltip-id={`tooltip-${index}`}
                                      data-tooltip-content="Export to .csv file!"
                                      data={allStudent[index]}
                                      className="btn px-4 arrow-btn border-danger"
                                      filename={allStudent[index][0].academy}
                                    >
                                      Export
                                    </CSVLink>
                                    <Tooltip id={`tooltip-${index}`} />
                                  </>
                                ) : (
                                  <span>Data not available</span>
                                )}
                                </div>
                            </div>

                            <div className='d-flex align-items-center'>
                                <h5 className=' ms-3 pt-4' style={{ color: '#888888' }}>Configuration</h5>
                                <FaArrowTurnDown className='mt-4 ms-3' color='#F04A00' size={25}/>
                            </div>
                            <div className='d-flex justify-content-between '>
                                <ProximityDiversity capacity = {item.capacity} proximity = {item.proximity} type = "configuration"/>
                                <ProximityRange proximityRange = {item.proximityRange}/>
                                <GenderBarGraph numberOfMale = {item.numberOfMale} numberOfFemale = {item.numberOfFemale} type = "configuration"/>
                            </div>
                            
                            <div className='d-flex align-items-center'>
                                <h5 className=' ms-3 pt-4' style={{ color: '#888888' }}>Actual Intake</h5>
                                <FaArrowTurnDown className='mt-4 ms-3' color='#F04A00' size={25}/>
                            </div>
                            <div className='d-flex justify-content-between '>
                                <ProximityDiversity capacity = {item.capacity} proximity = {item.proximity} type = "actual" students = {item.students}/>
                                <ProximityRange proximityRange = {item.proximityRange}/>
                                <Diversity east = {item.east} west = {item.west} south = {item.south} central = {item.central}/>
                                <GenderBarGraph numberOfMale = {item.numberOfMale} numberOfFemale = {item.numberOfFemale} students = {item.students} type = "actual"/>
                            </div>
                            <div className='table__preview mb-4'>
                                <TablePreview students = {item.students}/>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default Main;