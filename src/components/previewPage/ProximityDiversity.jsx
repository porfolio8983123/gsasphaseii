import React, { useEffect,useState } from 'react';
import { PieChart, Pie, Cell, Label } from 'recharts';

const ProximityDiversity = ({capacity,proximity,type,students}) => {

    const [totalCapacity,setTotalCapacity] = useState(0);
    
    const [data, setData] = useState([
        { name: 'Diversity', value: 10, fill: '#02344F' },
        { name: 'Proximity', value: 90, fill: '#F04A00' },
    ]);

    useEffect(() => {
        if (type === 'configuration') {
            setTotalCapacity(capacity);
            setData([
                { name: 'Diversity', value: totalCapacity-proximity, fill: '#02344F' },
                { name: 'Proximity', value: proximity, fill: '#F04A00' },
            ]);
        } else {
            let totalStudentCount = students.length;
            setTotalCapacity(totalStudentCount);
            setData([
                { name: 'Diversity', value: totalCapacity - proximity, fill: '#02344F' },
                { name: 'Proximity', value: proximity, fill: '#F04A00' },
            ]);
        }
    }, [totalCapacity]);

    const COLORS = ['#02344F', '#F04A00'];

    const RADIAN = Math.PI / 180;

    const renderCustomizedLabel = ({
        cx, cy, midAngle, innerRadius, outerRadius, percent,
    }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="white" textAnchor="middle" dominantBaseline="central" fontSize={12}>
                {`${(percent * totalCapacity).toFixed(0)}`}
            </text>
        );
    };

    return (
      
        <div className='main-container ms-4'>
            <div className='donut-chart-containe mt-5'>
                <p style={{color:'grey',fontSize:'16px',fontWeight:"400"}}>Proximity and Diversity Intake</p>
                <PieChart width={150} height={150} style={{marginTop:'-15px'}} margin={{ top: 0, right: 0, bottom: 0, left: 0 }}>
                    <Pie
                        data={data}
                        dataKey="value"
                        outerRadius={60}
                        innerRadius={30}
                        fill="#8884d8"
                        paddingAngle={0}
                        stroke="transparent"
                        labelLine={false}   
                        label={renderCustomizedLabel}
                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
            </div>
            <div className=' d-flex justify-content-between mt-3'>
                <div className='d-flex justify-content-start'>
                    <div style={{height:'20px',width:"20px",backgroundColor:"#F04A00"}}></div><p className='ms-2 mt-1'style={{fontSize:'12px'}}>Proximity</p>
                </div>
                <div className='d-flex justify-content-start '>
                    <div style={{height:'20px',width:"20px",backgroundColor:"#02344F"}}></div><p className='ms-2 mt-1'style={{fontSize:'12px'}}>Diversity</p>
                </div>
            </div>
        </div>
    );
};

export default ProximityDiversity;