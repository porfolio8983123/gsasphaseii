import React, { useState,useEffect } from 'react';
import { PieChart, Pie, Cell, Label } from 'recharts';

const COLORS = ['#02344F', '#EEB200', '#F04A00'];

const Diversity = ({east,west,south,central}) => {
    const [sliderValues, setSliderValues] = useState({
        South: 10,
        Central: 10,
        West: 10,
    });

    const data = [
        { name: 'South', value: sliderValues.South, fill: COLORS[0] },
        { name: 'Central', value: sliderValues.Central, fill: COLORS[1] },
        { name: 'West', value: sliderValues.West, fill: COLORS[2] },
    ];

    useEffect(() => {
        if (west === 0) {
            setSliderValues({
                South: south,
                Central: central,
                West: east,
              });
        } else if (central === 0) {
            setSliderValues({
                South: south,
                Central: east,
                West: west,
              });
        } else {
            setSliderValues({
                South: south,
                Central: central,
                West: west,
              });
        }
      }, [east, west, south, central]);
    

    return (
        <div className='d-flex'>
            <div className='mt-5 me-5'>
                <p className='text-start' style={{color:'grey',fontSize:'16px',fontWeight:"400"}}>Diversity Percentage Intake</p>
                <PieChart width={150} height={150} style={{marginTop:'-15px'}}> 
                    <Pie 
                        data={data}
                        dataKey="value"
                        nameKey="name"
                        outerRadius={60}
                        fill="#8884d8"
                        labelLine={false}
                        label={({
                            cx,
                            cy,
                            midAngle,
                            innerRadius,
                            outerRadius,
                            percent,
                            index,
                        }) => {
                            const RADIAN = Math.PI / 180;
                            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
                            const x = cx + radius * Math.cos(-midAngle * RADIAN);
                            const y = cy + radius * Math.sin(-midAngle * RADIAN);

                            return (
                                <text
                                    x={x}
                                    y={y}
                                    fill="#FFFFFF"
                                    textAnchor="middle"
                                    dominantBaseline="middle"
                                >
                                    {`${data[index].value}`}
                                </text>
                            );
                        }}
                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
                <div className='d-flex justify-content-between mt-3'>
                    <div className='d-flex justify-content-start '>
                        <div style={{height:'20px',width:"20px",backgroundColor:"#02344F"}}></div><div style={{display:'flex',flexDirection:'column'}}><p className='ms-2' style={{fontSize:'12px',marginTop:'-5px'}}>South</p><p className='ms-2' style={{fontSize:'12px',marginTop:'-20px'}}>({south})</p></div>
                    </div>
                    <div className='d-flex justify-content-start '>
                        <div style={{height:'20px',width:"20px",backgroundColor:"#EEB200"}}></div><div style={{display:'flex',flexDirection:'column'}}><p className='ms-2'style={{fontSize:'12px',marginTop:'-5px'}}>{central === 0?"East":"Central"}</p><p className='ms-2' style={{fontSize:'12px',marginTop:'-20px'}}>({central === 0?east:central})</p></div>
                    </div>
                    <div className='d-flex justify-content-start'>
                        <div style={{height:'20px',width:"20px",backgroundColor:"#F04A00"}}></div><div style={{display:'flex',flexDirection:'column'}}><p className='ms-2'style={{fontSize:'12px',marginTop:'-5px'}}>{west === 0? "East":"West"}</p><p className='ms-2' style={{fontSize:'12px',marginTop:'-20px'}}>({west === 0? east:west})</p></div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Diversity;