import React, { useState,useEffect } from 'react';
import { PieChart, Pie, Cell } from 'recharts';

const ProximityRange = ({proximityRange}) => {

    console.log("from proximity range ",proximityRange);

    const [sliderValue, setSliderValue] = useState(9); // Initial value for the slider

    const centerX = 125;
    const centerY = 125;

    useEffect(() => {
        // Update the slider value when proximityRange changes
        setSliderValue(proximityRange);
    }, [proximityRange]);

    const data = [
        { name: 'RangeIntake', value: sliderValue },
        { name: 'RangeLeft', value: 10 - sliderValue } // Ensuring the total value remains 10
    ];

    const COLORS = ['#FFBF00', 'rgba(45, 156, 219, 0.15)'];

    return (
        <div className='main-container mt-5 me-3'>
            <p style={{color:'grey',fontSize:'16px',fontWeight:"400"}} className='text-center'>Proximity Range Intake</p>
            <div className='donut-chart-container '>
                <PieChart width={250} height={250} style={{ marginTop: '-52px',}}>
                    <text x={centerX} y={centerY} fill="#000" textAnchor="middle" dominantBaseline="central" fontSize={20}>
                        1 - {data[0].value}
                    </text>
                    <Pie
                        data={data}
                        dataKey="value"
                        outerRadius={60}
                        innerRadius={30}
                        fill="#8884d8"
                        paddingAngle={0}
                        stroke="transparent"
                        labelLine={false}

                    >
                        {data.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index]} />
                        ))}
                    </Pie>
                </PieChart>
            </div>
        </div>
    );
};

export default ProximityRange;