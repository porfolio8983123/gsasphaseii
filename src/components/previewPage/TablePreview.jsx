import React, { useEffect, useState } from "react";
import Select from "react-select";
import ReactPaginate from 'react-paginate';
import '../../style/pagination.css';
import {GrNext,GrPrevious} from 'react-icons/gr';


function TablePreview({students,type}) {

    console.log("from table ", students);

    const [isVisible, setIsVisible] = useState(true);
    const [genderValue, setGenderValue] = useState("All");
    const itemsPerPage = 10;
    const [currentPage,setCurrentPage] = useState(0);
    const [searchCID,setSearchCID] = useState(null);
    const [allStudent,setAllStudent] = useState([]);

    useEffect(() => {
        setAllStudent(students);
    },[])

    const handlePageChange = ({ selected }) => {
        setCurrentPage(selected);
      };

    const offset = currentPage * itemsPerPage;
    const currentData = allStudent && allStudent.slice(offset, offset + itemsPerPage);

    const handleSearch = (e) => {
        e.preventDefault();
        // Perform search based on CID and gender

        console.log(genderValue.value)

        if (genderValue.value === 'All') {
            setAllStudent(students)
            return;
        }

        console.log("from search ",students[0])
        const filteredStudents = students.filter(student => {
            const matchesCID = !searchCID || student.CID.toString().includes(searchCID);
            return matchesCID;
        });
    
        console.log("filtered data ",filteredStudents);
        if (filteredStudents.length > 0) {
            setAllStudent(filteredStudents);
        } else {
            setAllStudent([])
        }
    };
    

    const genderOptions = [
        {value: "All", label: "All"},
        { value: "M", label: "Male" },
        { value: "F", label: "Female" },
    ];

    useEffect(() => {
        console.log(searchCID)
    },[searchCID])

    return (
        <div
            style={{
                padding: "5px",
                marginRight:'35px',
                marginLeft:'25px'
            }}
        >
            {isVisible ? (
                <div>
                    <button
                        onClick={() => setIsVisible(false)}
                        style={{
                        border: "none",
                        background: "none",
                        color: "#F04A00",
                        cursor: "pointer",
                        textDecoration: "underline",
                        marginRight: "10px",
                        display: type === 'setting' ? 'none' : '',
                        }}
                    >
                        Hide Enlistee
                    </button>
                    {/* filter  */}
                    <div>
                        <form
                            action=""
                            style={{
                                padding: "20px",
                                borderRadius: "10px",
                                display: "flex",
                                marginLeft: "-21px"
                            }}
                        >
                            <div className="mt-3">
                                <label className="text-drak">Search by CID</label>
                                <input name="cid" className="form-control" onChange={(e) => setSearchCID(e.target.value)}/>
                            </div>
                            <div
                                className="mt-4 mb-4 text-end ms-3" style={{ width: "120px" }}
                            >
                                <button
                                    className="btn form-control"
                                    style={{ backgroundColor: "#F04A00", color: "white", marginTop: "15px", }}

                                    onClick={handleSearch}
                                >
                                    Search
                                </button>
                            </div>
                        </form>
                    </div>

                    {/* table */}
                    <div style={{ marginTop: "-25px",}}>
                        {isVisible && (
                            <div
                               
                            >
                                <table style={{ borderRadius: "10px",  border:"none"}} className="table">
                                    <thead
                                        style={{
                                            position: "sticky",
                                            top: 0,
                                            backgroundColor: "#fff",
                                        }}
                                    >
                                        <tr className="table-secondary">
                                            <th>CID</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Dzongkha</th>
                                            <th>Gewog</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {currentData.map((student, index) => (
                                        <tr key={index}>
                                            <td>{student.CID}</td>
                                            <td>{student.name}</td>
                                            <td>{student.gender}</td>
                                            <td>{student.dzongkhag}</td>
                                            <td>{student.gewog}</td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                                <div className="d-flex justify-content-between align-items-center mb-3 mt-2">
                                    <ReactPaginate
                                        breakLabel={'...'}
                                        pageCount={Math.ceil(allStudent.length / itemsPerPage)}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={handlePageChange}
                                        containerClassName={'pagination'}
                                        activeClassName={'active'}
                                        pageClassName={'paginate-page'}
                                        nextLabel={<GrNext size={20} style={{marginTop:'6px'}}/>}
                                        previousLabel={<GrPrevious size={20} style={{marginTop:'6px'}}/>}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            ) : (
                <button onClick={() => setIsVisible(true)}
                style={{
                border: "none",
                background: "none",
                color: "#F04A00",
                cursor: "pointer",
                textDecoration: "underline",
                marginRight: "10px",
                display: type === 'setting' ? 'none' : '',
                }}>Show Enlistee</button>
            )}
        </div>

    );
}

export default TablePreview;