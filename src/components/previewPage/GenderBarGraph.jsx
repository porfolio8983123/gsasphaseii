import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell } from 'recharts';

const GenderBarGraph = ({numberOfMale,numberOfFemale,students,type}) => {

  const total = numberOfMale + numberOfFemale;
  const [totalMale,setTotalMale] = useState(0);
  const [totalFemale,setTotalFemale] = useState(0);

  const genderData = [
    { gender: 'Male', count: totalMale },
    { gender: 'Female', count: totalFemale },
    // Add more data points as needed
  ];

  const countGender = () => {
    if (students) {
      const genderCounts = students.reduce((counts, row) => {
        const gender = row['gender']; 
        counts[gender] = (counts[gender] || 0) + 1;
        return counts;
      }, {});

      console.log('Gender Counts:', genderCounts);

      setTotalMale(genderCounts.M);
      setTotalFemale(genderCounts.F);
    }

    console.log("from gender graph ",totalMale,totalFemale);
  }

  useEffect(() => {
    if (type === 'actual') {
      console.log("from gender graph ", students);
      countGender();
    } else {
      setTotalMale(numberOfMale);
      setTotalFemale(numberOfFemale);
    }
  },[])
  return (
    <div className='d-flex flex-column  justify-content-center align-items-center mt-5'>
      <p className='text-center' style={{color:'grey',fontSize:'16px',fontWeight:"400"}}>Gender Intake</p>
      <BarChart width={250} height={200} data={genderData} margin={{ top: 20, right: 30, left: 20, bottom: 5 }}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="gender" />
        <YAxis domain={[0, total]} />
        <Tooltip />
        <Bar dataKey="count" barSize={30}>
          {genderData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={entry.gender === 'Male' ? '#DFEFF9' : '#D9D9D9'} />
          ))}
        </Bar>
      </BarChart>
    </div>
  );
};

export default GenderBarGraph;