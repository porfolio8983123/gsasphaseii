import React, { useEffect } from 'react';
import Main from '../previewPage/Main';
import { AiOutlineLeft } from 'react-icons/ai';
import { useFetchAllocationQuery,useUpdateAllocationMutation } from '../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useState } from 'react';
import { CSVLink } from 'react-csv';
import Select from 'react-select';
import AcademyGraph from '../AcademyGraph';
import SkilingPreview from '../SkillingPreview';


const options = [
  {value: 0, label:"Academies"},
  {value: 1, label:"Skillings"},
  {value: 2, label:"ICT"},
  {value: 3, label:"CSS"},
  {value: 4, label:"HSS"},
  {value: 5, label:"FSS"}
]

function Preview({setPreviousStep,allocationName,allocationId,previewType,getAllStudentData,setNextStep}) {

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleSelectChange = (seleted) => {
    setSelectedOption(seleted);
  };

  useEffect(() => {
    console.log("selected filter ",selectedOption)
  },[selectedOption])

  const customStyles = {
    control: (provided) => ({
      ...provided,
      width: 150, // Set your desired fixed width here
      textAlign: 'left',
    }),
    option: (provided, state) => ({
      ...provided,
      textAlign: 'left',
      backgroundColor: state.isFocused ? '#F04A00' : 'white',
      color: state.isFocused ? 'white' : 'black',
      '&:hover': {
        backgroundColor: '#F04A00',
        color: 'white',
      },
    }),
  };

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center my-4" 
        style={{cursor:'pointer', marginRight:'35px',marginLeft:'30px'}}
      >
        {previewType && previewType === "dashboard" ? (
          <div>
            <h5>Enlistment Year 2024</h5>
          </div>
        ):(
          <div className="d-flex justify-content-center align-items-center"
            onClick={() => {
              setPreviousStep();
            }}
          >
            <AiOutlineLeft size={30} style={{ alignSelf: 'center' }}/> 
            <p className='ms-2' style={{ alignSelf: 'center', margin: 0, fontSize: '20px'}}>Allocation 1</p>
          </div>
        )

        }
        <div className="text-end me-3">
          <div className='d-flex'>
          <div className='d-flex align-items-center w-40 position-relative' style={{marginLeft:70}}>
            <div className='me-3'>
                <Select
                    options={options}
                    styles={customStyles}
                    value={selectedOption}
                    onChange={handleSelectChange}
                />
            </div>
            <div>
              <button className='btn px-5 me-5 arrow-btn border-danger' 
                    >Save</button>
              </div>
          </div>
            {/* {previewType && previewType === 'setting' &&
              <div className=''>
                  <button className="btn px-5 me-5 arrow-btn border-danger" disabled>
                    <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                </button>
                  <button className='btn px-5 me-5 arrow-btn border-danger' 
                  >Save</button>
              </div>
            } */}
          </div>
          </div>
        </div>
      <div
      style={{
        maxHeight: 'calc(100vh - 100px)',
        overflowY: 'scroll',
        marginRight: '35px',
        marginLeft: '30px',
        scrollbarWidth: 'thin',
      }}>
        {/* <Main data = {data}/> */}
       {selectedOption.value === 0 ? (
        <AcademyGraph/>
       ):(
        <>
         <SkilingPreview filterType = {selectedOption.label}/>
        </>
       )

       }
      </div>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        />
        {/* Same as */}
      <ToastContainer />
    </div>
  )
}

export default Preview
