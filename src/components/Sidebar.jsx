import {
    FaBars,
    FaTh,
    FaFileUpload,
    FaPlus,
  } from 'react-icons/fa';
  import { FaLocationDot } from 'react-icons/fa6';
  import { MdAccountCircle } from 'react-icons/md';
  import { IoLogOutOutline } from 'react-icons/io5';
  import { AiOutlineFileDone } from 'react-icons/ai';
  import { Outlet, useNavigate, useLocation } from 'react-router-dom';
  import logo from '../img/desung logo 1.png';
  import "../App.css";
  import { useEffect, useState } from 'react';
  import Cookies from 'js-cookie';
  import { useDispatch,useSelector } from 'react-redux';
  import { updateSideLine } from '../store';
  
  export default function Sidebar({ handleLogout }) {
  
    const navigate = useNavigate();
    const location = useLocation();

    const dispatch = useDispatch();
    const sideLineValue = useSelector((state) => state.sideLine);
  
    const [sideLine, setSideLine] = useState(0);
    const [token, setToken] = useState('');

    useEffect(() => {
      console.log("side line value is ",sideLineValue);
      setSideLine(sideLineValue.value);
    },[sideLineValue])

    useEffect(() => {
      console.log("side line changed occured ", sideLine);
    },[sideLine])
  
    const height = window.innerHeight;
  
    useEffect(() => {
      const token = Cookies.get('auth_token');
      setToken(token);
      setSideLine(0);
    }, [])
  
    const menuItem = [
      {
        path: '/Gyalsung',
        name: 'Dashboard',
        icon: <FaTh />
      },
      {
        path: 'fileupload',
        name: 'File upload',
        icon: <FaFileUpload />
      },
      {
        path: 'allocation',
        name: 'Allocation',
        icon: <FaPlus />
      },
      {
        path: 'academy',
        name: 'Academy',
        icon: <FaLocationDot />
      },
      {
        path: 'allocationrun',
        name: 'Allocation run',
        icon: <AiOutlineFileDone />
      },
      {
        path: 'report',
        name: 'Report',
        icon: <FaBars />
      },
      {
        path: 'profile',
        name: 'Profile',
        icon: <MdAccountCircle />
      },
      {
        path: 'logout',
        name: 'Log out',
        icon: <IoLogOutOutline />
      }
    ];
  
    const handleMenuItemClick = async (item, index) => {
      dispatch(updateSideLine(index));
  
      if (item.path === 'logout') {
        await fetch('http://localhost:8000/api/logout', {
          method: 'POST',
          headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
          }
        })
          .then((response) => {
            if (response.ok) {
              return response.json();
            } else {
              console.log("Failed")
            }
          })
          .then((data) => {
            Cookies.remove('auth_token');
            handleLogout();
            console.log(data);
          })
          .catch((error) => {
            console.log(error.message);
          })
      } else {
        navigate(item.path);
      }
    }
  
    return (
      <div className="nav__container">
        <div className="nav__sidebar">
          <div className="top__top_section">
            <img src={logo} alt="Logo" />
          </div>
          <div>
            <div className='nav__title'>
              <p style={{ textAlign: 'center', marginTop: -25 }}>Gyalsung Smart Allocation</p>
              <p className='sidebar__system'>System</p>
            </div>
          </div>
          {
            menuItem.map((item, index) => (
              <div
                key={index}
                className={`${height <= 550 ? "nav__link__for__small" : "nav__link"} ${location.pathname === item.path ? 'active' : ''}`}
                onClick={() => handleMenuItemClick(item, index)}
                style={{ cursor: 'pointer' }}
              >
                <div className={sideLine === index ? "nav__line" : ''}></div>
                <div className='nav__icon'>{item.icon}</div>
                <div className='nav__link_text'>{item.name}</div>
              </div>
            ))
          }
        </div>
        <main>
          <Outlet/>
        </main>
      </div>
    );
  }
  