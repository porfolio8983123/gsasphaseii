
import logo from "../img/Group 119.png"
import { useState,useEffect } from "react";

import "../style/Topheader.css"
import { useFetchAdminQuery } from "../store/apis/profileApi";

function Dashboard() {

  const [windowWidth, setWindowWidth] = useState(window.innerWidth - 295 + 20);
  console.log(windowWidth)

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth - 295 + 20);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const [userName,setUserName] = useState('Soname');
  const [userString,setUserString] = useState('');

  useEffect(() => {
    let u = "";
    const user = userName.split(" ");
    user.forEach(element => {
      u+=element.charAt(0);
    });

    setUserString(u);
  },[userName])

  return (
    <div className='header__container' style={{width:`${windowWidth}px`}}>
      <div  className='profile__container'>
        <div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
          <h5>{userName}</h5>
          <div className="profile__admin" style={{width: 38,height:38}}>
            {userString}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;

