import React, { useState } from 'react';
import { PieChart, Pie, Cell } from 'recharts';
import './SettingDiversity.css';
import { useDispatch,useSelector } from 'react-redux';
import { setKhotokhaEast,setKhotokhaSouth,setKhotokhaWest } from '../../../store';
import { useEffect } from 'react';
import { useFetchAcademiesQuery } from '../../../store';

const COLORS = ['#02344F', '#EEB200', '#00CED1'];

const SettingDiversity = () => {

    const dispatch = useDispatch();
    const [proximity,setProximity] = useState();
    const [percentage,setPercentage] = useState();
    const [proximityVariation,setProximityVariation] = useState(false);

    const khotokhaData = useSelector((state)=> state.khotokha)

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [khotokhaCapacity,setKhotokhaCapacity] = useState(0);

    useEffect(() => {
        if (data) {
        // Check if data.academies is defined before accessing it
        if (data.academies) {
            data.academies.forEach(element => {
            if (element.name === "Khotokha Academy") {
                setKhotokhaCapacity(element.capacity);
                return;
            }
            });
        }
        }
    }, [data]);

    console.log("from diversity ", khotokhaData);

    const [sliderValues, setSliderValues] = useState({
        South: 0,
        West: 0,
        East: 0,
    });

    useEffect(() => {
        setSliderValues({
            South:khotokhaData.south, 
            West:khotokhaData.west, 
            East: khotokhaData.east
        })
    },[khotokhaData,data])

    useEffect(() => {
        setProximity(khotokhaData.proximity)
        if (proximity) {
            setPercentage(khotokhaCapacity-proximity);
        }

        if (percentage < sliderValues.South || percentage < sliderValues.West ||percentage < sliderValues.East) {
            setProximityVariation(true);
        }
    },[khotokhaData,percentage,proximity,data])

    useEffect(() => {
        if (proximityVariation === true) {
            setSliderValues({
                South: 0,
                West: 0,
                East: 0,
            })
    
            dispatch(setKhotokhaWest(0))
            dispatch(setKhotokhaEast(0))
            dispatch(setKhotokhaSouth(0));
        }
    },[proximityVariation])

    console.log("Promity ",proximity);

    const handleSliderChange = (name, value) => {
        console.log("handling change slider ",value);

        const total = sliderValues.South + sliderValues.West + sliderValues.East - sliderValues[name];

        // Distribute the remaining value evenly to other sliders
        if (total + value <= percentage) {
            const updatedValues = {
                South: name === 'South' ? value : sliderValues.South,
                West: name === 'West' ? value : sliderValues.West,
                East: name === 'East' ? value : sliderValues.East,
            };
    
            setSliderValues(updatedValues);
            dispatch(setKhotokhaWest(parseInt(updatedValues.West)))
            dispatch(setKhotokhaEast(parseInt(updatedValues.East)))
            dispatch(setKhotokhaSouth(parseInt(updatedValues.South)));
        }
    };

    const kdata = [
        { name: 'South', value: sliderValues.South, fill: COLORS[0] },
        { name: 'West', value: sliderValues.West, fill: COLORS[1] },
        { name: 'East', value: sliderValues.East, fill: COLORS[2] },
    ];

    return (
        <div className={`${khotokhaData.proximity === khotokhaCapacity ?'d-none':''}`}>
            <h6 className='ms-5 ps-5'>Diversity Intake Percentage</h6>
            <div className='d-flex justify-content-evenly align-item-center' style={{marginTop:"-5%"}}>
            
                <PieChart width={400} height={400}> 
                    <Pie 
                        data={kdata}
                        dataKey="value"
                        nameKey="name"
                        outerRadius={80}
                        fill="#8884d8"
                        labelLine={false}
                        label={({
                            cx,
                            cy,
                            midAngle,
                            innerRadius,
                            outerRadius,
                            percent,
                            index,
                        }) => {
                            const RADIAN = Math.PI / 180;
                            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
                            const x = cx + radius * Math.cos(-midAngle * RADIAN);
                            const y = cy + radius * Math.sin(-midAngle * RADIAN);

                            return (
                                <text
                                    x={x}
                                    y={y}
                                    fill="#FFFFFF"
                                    textAnchor="middle"
                                    dominantBaseline="middle"
                                >
                                    {`${kdata[index].name}`}
                                </text>
                            );
                        }}
                    >
                        {kdata.map((entry, index) => (
                            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                        ))}
                    </Pie>
                </PieChart>
                <div className='d-flex flex-column align-item-center justify-content-center me-5 px-5'style={{width:"35%"}}>
                    {proximity? (
                        <h4>Central {proximity}</h4>
                    ):(
                        <h4>Central 0</h4>
                    )}
                    <div className='d-flex '>
                        <label>{parseInt(sliderValues.South)}</label>
                        <input 
                            className='w-100 slider1 mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            value={sliderValues.South}
                            onChange={(e) => {
                                console.log("south data ",e.target.value)
                                handleSliderChange('South', parseInt(e.target.value, 10))
                            }}
                            style={{ background: COLORS[0] }} // Set background color to #02344F
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                    <div className='d-flex mt-2  w-100'>
                        <label>{parseInt(sliderValues.West)}</label>
                        <input
                            className='w-100 slider2 mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            value={sliderValues.West}
                            onChange={(e) => handleSliderChange('West', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[1], backgroundColor: '#EEB200' }} // Set background color to #EEB200
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                    <div className='d-flex mt-2'>
                        <label>{parseInt(sliderValues.East)}</label>
                        <input
                            className='w-100 slider3  mt-1'
                            type="range"
                            min="0"
                            max={percentage}
                            
                            value={sliderValues.East}
                            onChange={(e) => handleSliderChange('East', parseInt(e.target.value, 10))}
                            style={{ background: COLORS[2] }} // Set background color to #F04A00
                        />
                        <label>{percentage && percentage !== undefined ? `${percentage}`:"0"}</label>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SettingDiversity;
