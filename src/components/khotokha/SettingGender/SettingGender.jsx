import React, { useState } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { setKhotokhaNumberOfFemale,setKhotokhaNumberOfMale,setKhotokhaEastMale,setKhotokhaProximityMale,setKhotokhaSouthMale,setKhotokhaWestMale } from '../../../store';
import { useEffect } from 'react';
import { useFetchAcademiesQuery } from '../../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SettingGender = ({settingKhotokhaGenderError}) => {

    const dispatch = useDispatch();

    const khotokhaData = useSelector((state) => state.khotokha);

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [khotokhaCapacity,setKhotokhaCapacity] = useState(0);

    useEffect(() => {
        if (data) {
        // Check if data.academies is defined before accessing it
        if (data.academies) {
            data.academies.forEach(element => {
            if (element.name === "Khotokha Academy") {
                setKhotokhaCapacity(element.capacity);
                return;
            }
            });
        }
        }
    }, [data]);

    console.log("from gender ",khotokhaData);

    const [maleCount, setMaleCount] = useState(0);
    const [femaleCount, setFemaleCount] = useState(0);
    const [southMaleCount,setSouthMaleCount] = useState(0);
    const [proximityMaleCount,setProximityMaleCount] = useState(0);
    const [westMaleCount,setWestMaleCount] = useState(0);
    const [eastMaleCount,setEastMaleCount] = useState(0);

    const [maleExceed,setMaleExceed] = useState(false);

    useEffect(() => {
        let total = maleCount + femaleCount;
        if (total > khotokhaCapacity || total < khotokhaCapacity) {
            settingKhotokhaGenderError(true);
            setMaleExceed(true);
            toast.error('Total student exceeding or lower the capacity!', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        } else {
            setMaleExceed(false);
            settingKhotokhaGenderError(false);
        }
    },[maleCount,femaleCount])

    const handleMaleCountChange = (e) => {
        setMaleCount(parseInt(e.target.value));
        dispatch(setKhotokhaNumberOfMale(parseInt(e.target.value)));
    };

    useEffect(() => {
        setMaleCount(khotokhaData.numberOfMale)
        setFemaleCount(khotokhaData.numberOfFemale)
        setSouthMaleCount(khotokhaData.southMale)
        setWestMaleCount(khotokhaData.westMale)
        setProximityMaleCount(khotokhaData.centralMale)
        setEastMaleCount(khotokhaData.eastMale)
    },[khotokhaData])

    const handleFemaleCountChange = (e) => {
        setFemaleCount(parseInt(e.target.value));
        dispatch(setKhotokhaNumberOfFemale(parseInt(e.target.value)));
    };

    const handleSouthMaleChange = (e) => {
        setSouthMaleCount(parseInt(e.target.value,10));
        dispatch(setKhotokhaSouthMale(parseInt(e.target.value)));
    }

    const handleProximityMaleChange = (e) => {
        setProximityMaleCount(parseInt(e.target.value,10));
        dispatch(setKhotokhaProximityMale(parseInt(e.target.value)));
    }

    const handleWestMaleChange = (e) => {
        setWestMaleCount(parseInt(e.target.value,10));
        dispatch(setKhotokhaWestMale(parseInt(e.target.value)));
    }

    const handleEastMaleChange = (e) => {
        setEastMaleCount(parseInt(e.target.value,10));
        dispatch(setKhotokhaEastMale(parseInt(e.target.value)));
    }

    // useEffect(() => {
    //     const total = westMaleCount + proximityMaleCount + eastMaleCount + southMaleCount;

    //     console.log("total male from diversity and proximity ",total);
        
    //     if (parseInt(total) > maleCount) {
    //         setMaleExceed(true);
    //         toast.error('Total male exceeding!', {
    //             position: "top-right",
    //             autoClose: 5000,
    //             hideProgressBar: false,
    //             closeOnClick: true,
    //             pauseOnHover: true,
    //             draggable: true,
    //             progress: undefined,
    //             theme: "light",
    //         });
    //     } else {
    //         setMaleExceed(false);
    //     }

    // },[westMaleCount,southMaleCount,proximityMaleCount,eastMaleCount])


    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <h6>Capacity: {khotokhaCapacity?khotokhaCapacity:0}</h6>
            <form action="" className="mt-4">
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                {/* <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Proximity Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={proximityMaleCount}
                        onChange={handleProximityMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">South Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={southMaleCount}
                        onChange={handleSouthMaleChange}
                    />
                </div>
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">West Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={westMaleCount}
                        onChange={handleWestMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">East Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={eastMaleCount}
                        onChange={handleEastMaleChange}
                    />
                </div> */}
            </form>
        </div>
    );
};

export default SettingGender;