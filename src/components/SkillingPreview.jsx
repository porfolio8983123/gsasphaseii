import * as React from 'react';
import { useState, useEffect } from 'react';
import AcademicDiversityPreview from './previewComponents/AcademicDiversityPreview'
import GenderPieChart from './previewComponents/GenderPieChart';
import BarGraphPreview from './previewComponents/BarGraphPreview';
import AllocationTable from './tables/AllocationTable';

export default function SkilingPreview({filterType}) {
  const [filteredData, setFilteredData] = useState([]);
  const skilling = [
    { academy: 'Gyalpozhing Academy', name: "ICT", MathBenchMark: "60", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "2000" },
    { academy: 'Pemathang Academy', name: "CSS", EnglishBenchMark: "55", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Khotokha Academy', name: "CSS", EnglishBenchMark: "55", NoAcademiDiversity: false, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Jamtsholing Academy', name: "HSS", NoAcademiDiversity: true, data: "table", totalEnlisteeEnrolled: "1000" },
    { academy: 'Jamtsholing Academy', name: "FSS", NoAcademiDiversity: true, data: "table", totalEnlisteeEnrolled: "1000" }
  ];

  useEffect(() => {
    if (filterType === "Skillings") {
      setFilteredData(skilling);
    } else {
      const filterData = skilling.filter((value) => value.name === filterType);
      setFilteredData(filterData);
      console.log("filterd data ",filterData);
    }
  },[filterType])

  // Modify the MathBenchMark for Pemathang Academy and Khotokha Academy
  skilling[1].EnglishBenchMark = skilling[1].MathBenchMark;
  skilling[2].EnglishBenchMark = skilling[2].MathBenchMark;

  return (
    <>
      {filteredData.map((skill, index) => (
        <div className="container mt-4 p-3" key={index}>
          <div className='d-flex justify-content-between'>
            <div>
              <h5 style={{}} className='text-start ms-5'>{skill.academy} ({skill.name})</h5>
              <hr className='ms-5 rounded' style={{ width: "90%", height: "3px" }}></hr>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-4'>
                <p className='h6'>TotalEnlisteeEnrolled:</p>
            </div>
            <div className='col-md-4'>
              <p className='h6'>Total Male Enrolled:</p>
            </div>
            <div className='col-md-4'>
              <p className='h6'>Total Female Enrolled:</p>
            </div>
            

          </div>
          <div className="row mt-3">
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4">
                <div>
                  <p>Academic Mark and Diversity Intake</p>
                </div>
                <AcademicDiversityPreview />
              </div>
            )}
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4">
                <div>
                  <p>Gender Intake</p>
                </div>
                <GenderPieChart />
              </div>
            )}
            {!skill.NoAcademiDiversity && (
              <div className="col-md-4 mb-4">
                <div className=''>
                  <p style={{ fontSize: '20px' }} className=''>Benchmark for<br /> Academic {skill.name === 'ICT' ? 'Math' : 'English'}:</p>
                </div>
                <p style={{ fontSize: '70px', color: '#F04A00' }}>{skill.name === 'ICT' ? '60%' : (skill.EnglishBenchMark ? skill.EnglishBenchMark + '%' : '55%')}</p>
              </div>
            )}
          <div className="col-md-12 mb-12 mt-3">
            <p className='text-center'>Enlistee from each Dzongkhag</p>
              <div className='d-flex justify-content-center align-items-center'>
                <BarGraphPreview />
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-md-12 mb-12">
              <AllocationTable/>
            </div>
          </div>
        </div>
      ))}
    </>
  );
}
