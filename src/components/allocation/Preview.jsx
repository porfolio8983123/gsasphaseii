import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import ReactPaginate from 'react-paginate';
import graduateCap from '../../img/game-icons_graduate-cap.png';
import { addStudentData, updateData } from '../../store';
import { useDispatch } from 'react-redux';
import { useAddStudentMutation } from '../../store';
import EnlisteeUploadModal from '../modals/EnlisteeUploadModal';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { setTotalCentral,setTotalEast,setTotalFemale,setTotalMale,setTotalSouth,setTotalWest,setTotalFemaleCentral,setTotalFemaleEast,setTotalFemaleSouth,setTotalFemaleWest,setTotalMaleCentral,setTotalMaleEast,setTotalMaleSouth,setTotalMaleWest } from '../../store';
import {GrNext,GrPrevious} from 'react-icons/gr';
import '../../style/pagination.css';
import FileUploadPreview from '../fileUpload/FileUploadPreview';

export default function Preview({setNextStep,setPreviousStep}) {

    const overlayStyle = {
      position: "fixed",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      backgroundColor: "rgba(0, 0, 0, 0.5)", // Semi-transparent black background
      display: "none",
      zIndex: 0, // Set a lower zIndex than the modal to prevent interaction with elements behind
    };

  return (
    <div>
      <div style={overlayStyle}></div> {/* Overlay */}
      <ToastContainer
          position="top-right"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
          theme="light"
      />
      <div className="grid-container" style={{display: 'grid', gridTemplateColumns: '3fr 1fr' }}>
        <div style={{marginTop:'30px'}}>
          <div className="left-panel" style={{ borderRight: '1px solid #ccc', padding: '20px',maxHeight: 'calc(100vh - 200px)', overflowY: 'auto' }}>
            <div className="container shadow p-3 bg-body rounded">
            <div style={{marginTop:'-30px'}}>
            {/* {csvData?.length > 0 &&
              <FileUploadPreview students={csvData} />
            } */}
            Hello
          </div>
            </div>
            <div className='d-flex justify-content-between mt-5' style={{marginBottom:100}}>
              <button onClick={setPreviousStep} className='btn' style={{backgroundColor:'#F04A00',color:"white"}}>Previous</button>
              <button className="btn" onClick={setNextStep} style={{ backgroundColor: '#F04A00', color: 'white' }}>
                Next
              </button>
            </div>
          </div>
        </div>
        <div className="right-panel" style={{ padding: '10px',marginRight:'-20px',marginTop:'35px' }}>
        <div action="" style={{ backgroundColor: '#02344F',padding:'20px',borderRadius:'10px',color:'white' }}>
          <p className='text-center'><img src={graduateCap} alt='graduate cap' style={{width:'30px',height:'30px'}}/></p>
          <h5 className='text-center text-light'>Enlistee Information</h5>
          <div className='mt-4 text-center'>
            <h6>Student Data</h6>
            <div className='d-flex justify-content-center' style={{marginTop:'-15px'}}>
              <hr style={{width:'100px'}}/>
            </div>
            <h6>Total Student: <span style={{color:"#F04A00"}}>9842</span></h6>
            <h6>Total Male: <span style={{color:"#F04A00"}}>4000</span></h6>
            <h6>Total Female: <span style={{color:"#F04A00"}}>4000</span></h6>
          </div>
        </div>
      </div>
      </div>
      {/* <EnlisteeUploadModal openModal={dataUploading} toggleModal={cancelDataStoringProcess}/> */}
    </div>
  )
}
