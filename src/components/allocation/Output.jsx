import React, { useState,useEffect } from 'react';
import TablePreview from '../previewPage/TablePreview';
import { useNavigate } from 'react-router-dom';
import { CSVLink } from 'react-csv';
import { updateSideLine } from '../../store';
import { useDispatch } from 'react-redux';

export default function Output({setNextStep,setPreviousStep,allStudentData,type}) {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [allStudent,setAllStudent] = useState([]);

  useEffect(() => {
    
    let allData = [];

    allStudentData[0].students.forEach(element => {
      let object = {
        CID: element.CID,
        name:element.name,
        gender: element.gender,
        dzongkhag:element.dzongkhag,
        gewog:element.gewog,
        academy:allStudentData[0].name,
        zone:element.zone
      }

      allData.push(object);
    });



    allStudentData[1].students.forEach(element => {
      let object = {
        CID: element.CID,
        name:element.name,
        gender: element.gender,
        dzongkhag:element.dzongkhag,
        gewog:element.gewog,
        academy:allStudentData[1].name,
        zone:element.zone
      }

      allData.push(object);
    });

    allStudentData[2].students.forEach(element => {
      let object = {
        CID: element.CID,
        name:element.name,
        gender: element.gender,
        dzongkhag:element.dzongkhag,
        gewog:element.gewog,
        academy:allStudentData[2].name,
        zone:element.zone
      }

      allData.push(object);
    });

    allStudentData[3].students.forEach(element => {
      let object = {
        CID: element.CID,
        name:element.name,
        gender: element.gender,
        dzongkhag:element.dzongkhag,
        gewog:element.gewog,
        academy:allStudentData[3].name,
        zone:element.zone
      }

      allData.push(object);
    });

    setAllStudent(allData);
  },[allStudentData])

  console.log('from output ',allStudent);

  return (
    <div>
        <div className='d-flex justify-content-end me-4'>
          <div
            className='mb-2 d-flex justify-content-end mt-5'
          ><CSVLink className='btn px-4 arrow-btn border-danger me-5' data = {allStudent} filename={'allAcademyStudent.csv'}>Export All</CSVLink></div>
          <div
            className='mb-2 d-flex justify-content-end mt-5'
          ><button className='btn px-4 arrow-btn border-danger me-5' 
            onClick={() => {
              console.log("clicking button")
              navigate('/Gyalsung');
              dispatch(updateSideLine(0))
            }}
          >Complete</button></div>
        </div>
        <div style={{maxHeight: 'calc(100vh - 300px)',
          overflowY: 'scroll',marginBottom:'500px'}}>
          {allStudentData && allStudentData.length > 0 &&
            allStudentData.map((item,index) => (
              <div key={index}>
                <h5>{item.name}</h5>
                <TablePreview students={item.students} type = {type}/>
              </div>
            ))
          }
        </div>
    </div>
  )
}
