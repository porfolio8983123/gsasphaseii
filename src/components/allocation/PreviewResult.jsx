import React from 'react'

export default function PreviewResult({setNextStep,setPreviousStep}) {
  return (
    <div>
        <h1>PreviewResult</h1>
        <button onClick={setPreviousStep}>Previous</button>
        <button onClick={setNextStep}>Next</button>
    </div>
  )
}
