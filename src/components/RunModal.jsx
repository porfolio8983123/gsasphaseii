import { IoIosCloseCircle } from "react-icons/io";

export const RunModal = ({showModal,settingShowModal,handleCloseModal,allocationName,settingAllocationName, settingAcademyIndex, academyIndex, academySkillingData}) => {

    console.log("academyskilling ",academySkillingData);

    return (
        <>
             {showModal && <div className="modal-overlay" onClick={() => {
                handleCloseModal(false)
             }} style={{position:'fixed',top:0,left:0,width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,0.5)',zIndex:'999'}}></div>}

            {/* Bootstrap Modal */}
            <div className="modal mt-5 pt-5" tabIndex="-1" role="dialog" style={{ display: showModal ? 'block' : 'none' }}>
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className='d-flex justify-content-end'><IoIosCloseCircle size={40} color='#F04A00'
                    onClick={() => settingShowModal(false)}
                    style={{cursor:'pointer'}}
                /></div>
                <div className="mt-4">
                    <h4 className="text-center">Do you want to run this<br/> allocation for {academySkillingData?.name} ?</h4>
                </div>
                {academyIndex === 0 &&
                    <div className="">
                
                        <form action="">
                        <div className='d-flex justify-content-center mt-4 p-3'>
                            <div className='px-2'>
                            <label htmlFor="" className='h4 mt-1'>Name</label>
                            </div>
                            <input type="text" value={allocationName} onChange={(e) => settingAllocationName(e.target.value)} placeholder='Enter allocation name' className='form-control' style={{width:'60%'}}/>
                        </div>
                        </form>
                    </div>
                }
                <div className=" mt-3  px-5 mb-3">
                    <div className='d-flex justify-content-center'>
                    <div className='p-2 d-flex justify-content-center' style={{width:'100%'}}>
                        <button type="button" className="btn px-5 py-2" style={{backgroundColor:'#F04A00',color:"white"}}
                            onClick={() => {
                                settingAcademyIndex()
                            }}
                        >
                            Confirm
                        </button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </>
    )
}