import React, { useState } from 'react';

export default function EnlisteeUploadModal({openModal,toggleModal}) {

  const popupStyle = {
    position: "fixed",
    top: "50%",
    left: "55%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#ffffff",
    textAlign: "center",
    zIndex: 1, // Ensure the modal is above the overlay
    paddingTop:"50px",
    paddingBottom:'50px',
    paddingLeft:"20px",
    paddingRight:"20px",
    borderRadius:'10px'
  };

  return (
    <div>
        {openModal && (
          <div style={{borderRadius:'20px'}}>
            <div className='' style={popupStyle}>
              <h5>Data Uploading</h5>
              <div className='mx-5 mt-3' style={{}}>
                <div className="spinner-border" style={{ width: "4rem", height: "4rem", borderWidth: "7px",color:'#F04A00' }} role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </div>
              {/* <div className='mx-3' style={{ display: "flex", justifyContent: "end"}}>
                <button
                  style={{
                    marginLeft: 20,
                    width: "170px",
                    height: "40px",
                    borderRadius: "5px",
                    border: "1px solid #F04A00"
                  }}
                  onClick={toggleModal}
                >
                  Cancel
                </button>
              </div> */}
            </div>
          </div>
        )}
      </div>
  );
}
