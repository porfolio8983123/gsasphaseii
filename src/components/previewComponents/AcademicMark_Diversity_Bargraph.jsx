import React from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

export default function AcademicDiversityBars() {
  const data = [
    { name: 'Thim', TotalEnlistee: 4,  },
    { name: 'Paro', TotalEnlistee: 3, },
    { name: 'Haa', TotalEnlistee: 2, },
    { name: 'Tgang', TotalEnlistee: 4, },
    { name: 'Tyang', TotalEnlistee: 3,},
    { name: 'Sjong', TotalEnlistee: 2, },
    { name: 'Chuk', TotalEnlistee: 4, },
    { name: 'Trong', TotalEnlistee: 3,},
    { name: 'Wang', TotalEnlistee: 2, },
    { name: 'Puna', TotalEnlistee: 4,  },
    { name: 'Gasa', TotalEnlistee: 3, },
    { name: 'Bumt', TotalEnlistee: 2, },
    { name: 'Mong', TotalEnlistee: 4,  },
    { name: 'Daga', TotalEnlistee: 3, },
    { name: 'Tsir', TotalEnlistee: 2,  },
    { name: 'Sarp', TotalEnlistee: 4, },
    { name: 'Zhem', TotalEnlistee: 3,  },
    { name: 'Pemag', TotalEnlistee: 2,  },
    { name: 'Lhue', TotalEnlistee: 4,  },
    { name: 'Sam', TotalEnlistee: 3,  },
    
  ];

  return (
    <div style={{ width: '100%', height: 400 }}> {/* Set a fixed height for the container */}
      <ResponsiveContainer width="100%" height="100%">
        <BarChart data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" angle={-90} textAnchor="end" interval={0} tick={{ dy: 10 }} height={60}/>
          <YAxis />
          <Tooltip />
          <Bar dataKey="TotalEnlistee" fill="rgba(240,74,0,0.8)" />
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
}
