import React, { useState } from 'react';
import { PieChart, pieArcLabelClasses } from '@mui/x-charts/PieChart';
import Slider from '@mui/material/Slider';

const initialData = [
  { id: 0, value: 50, label: 'Academic Bench Mark' },
  { id: 1, value: 50, label: '' },
];

export default function AcademicBenchMark() {

  const [data, setData] = useState(initialData);

  const totalValue = data.reduce((acc, curr) => acc + curr.value, 0);

  const handleSliderChange = (event, newValue) => {
    const newValueArray = [newValue, 100 - newValue];
    const newData = data.map((item, index) => ({
      ...item,
      value: newValueArray[index],
    }));
    setData(newData);
  };

  let dataPercentages;
  if (totalValue === 0) {
    dataPercentages = data.map(item => ({
      ...item,
      value: 50,
      color: item.label === 'Diversity' ? '#FFBF00' : 'rgba(45,156,219,0.15)',
    }));
  } else {
    dataPercentages = data.map((item, index) => ({
      ...item,
      value: (item.value / totalValue) * 100,
      color: index === 0 ? '#FFBF00' : 'rgba(45,156,219,0.15)',
    }));
  }

  const academicBenchmarkPercentage = data.find(item => item.label === 'Academic Bench Mark').value;

  return (
    <div>
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <PieChart
          margin={{ left: 100 }}
          series={[
            {
              innerRadius: 30,
              data: dataPercentages,
              highlightScope: { faded: 'global', highlighted: 'item' },
              faded: { innerRadius: 30, additionalRadius: -30, color: 'gray' },
            },
          ]}
          slotProps={{
            legend: {
              hidden: true // Hide legend
            }
          }}
          sx={{
            [`& .${pieArcLabelClasses.root}`]: {
              fill: 'white',
              fontWeight: 'bold',
            },
          }}
          height={160}
          className="custom-pie-chart"
        >
          <text x="50%" y="50%" textAnchor="middle" dominantBaseline="middle" fontSize="20px">{academicBenchmarkPercentage}%</text>
        </PieChart>
      </div>
      <div style={{ display: 'flex', justifyContent: 'center', marginTop: '20px' }}>
        <p>Bench Mark</p>
      </div>
    </div>
  );
}
