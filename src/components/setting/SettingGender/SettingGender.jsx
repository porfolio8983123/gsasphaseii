import React, { useState,useEffect } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import { setNumberOfFemale,setNumberOfMale,setCentralMale,setSouthMale,setProximityMale,setWestMale } from '../../../store';
import { useFetchAcademiesQuery } from '../../../store';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const SettingGender = ({settingGenderError}) => {

    const dispatch = useDispatch();

    const gyalpozhingData = useSelector((state) => state.gyalpozhing);

    const {data,isLoading,error,refetch} = useFetchAcademiesQuery();

    const [gyalpozhingCapacity,setGyalpozhingCapacity] = useState(0);

    useEffect(() => {
        if (data) {
        // Check if data.academies is defined before accessing it
            if (data.academies) {
                data.academies.forEach(element => {
                if (element.name === "Gyalpozhing Academy") {
                    setGyalpozhingCapacity(element.capacity);
                    return;
                }
            });
        }
        }
    }, [data]);

    console.log("from gender ",gyalpozhingData);

    const [maleCount, setMaleCount] = useState(0);
    const [femaleCount, setFemaleCount] = useState(0);
    const [southMaleCount,setSouthMaleCount] = useState(0);
    const [proximityMaleCount,setProximityMaleCount] = useState(0);
    const [westMaleCount,setWestMaleCount] = useState(0);
    const [centralMaleCount,setCentralMaleCount] = useState(0);

    const [maleExceed,setMaleExceed] = useState(false);

    useEffect(() => {
        if (gyalpozhingCapacity !== 0) {
            let total = maleCount + femaleCount;
            if (total > gyalpozhingCapacity || total < gyalpozhingCapacity) {
                settingGenderError(true);
                setMaleExceed(true);
                toast.error('Total student exceeding or lower the capacity!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: "light",
                });
            } else {
                setMaleExceed(false);
                settingGenderError(false);
            }
        }
    },[maleCount,femaleCount])

    useEffect(() => {
        setMaleCount(gyalpozhingData.numberOfMale)
        setFemaleCount(gyalpozhingData.numberOfFemale)
        setSouthMaleCount(gyalpozhingData.southMale)
        setWestMaleCount(gyalpozhingData.westMale)
        setProximityMaleCount(gyalpozhingData.eastMale)
        setCentralMaleCount(gyalpozhingData.centralMale)
    },[gyalpozhingData])

    const handleMaleCountChange = (e) => {
        setMaleCount(parseInt(e.target.value,10));
        dispatch(setNumberOfMale(parseInt(e.target.value)));
        // const totalStudent = parseInt(maleCount + femaleCount);

        // console.log("ts",totalStudent,e.target.value);

        // if (totalStudent < gyalpozhingCapacity) {
        //     toast.error('Total student exceeding the capacity!', {
        //         position: "top-right",
        //         autoClose: 5000,
        //         hideProgressBar: false,
        //         closeOnClick: true,
        //         pauseOnHover: true,
        //         draggable: true,
        //         progress: undefined,
        //         theme: "light",
        //         });
        // } else {
        //     setMaleCount(e.target.value);
        //     dispatch(setNumberOfMale(parseInt(e.target.value)));
        // }
    };

    const handleFemaleCountChange = (e) => {
        setFemaleCount(parseInt(e.target.value,10));
        dispatch(setNumberOfFemale(parseInt(e.target.value)));
    };

    const handleSouthMaleChange = (e) => {
        setSouthMaleCount(parseInt(e.target.value,10));
        dispatch(setSouthMale(parseInt(e.target.value)));
    }

    const handleProximityMaleChange = (e) => {
        setProximityMaleCount(parseInt(e.target.value,10));
        dispatch(setProximityMale(parseInt(e.target.value)));
    }

    const handleWestMaleChange = (e) => {
        setWestMaleCount(parseInt(e.target.value,10));
        dispatch(setWestMale(parseInt(e.target.value)));
    }

    const handleCentralMaleChange = (e) => {
        setCentralMaleCount(parseInt(e.target.value,10));
        dispatch(setCentralMale(parseInt(e.target.value)));
    }

    // useEffect(() => {
    //     const total = westMaleCount + proximityMaleCount + centralMaleCount + southMaleCount;

    //     console.log("total male from diversity and proximity ",total);
        
    //     if (parseInt(total) > maleCount) {
    //         setMaleExceed(true);
    //         toast.error('Total male exceeding!', {
    //             position: "top-right",
    //             autoClose: 5000,
    //             hideProgressBar: false,
    //             closeOnClick: true,
    //             pauseOnHover: true,
    //             draggable: true,
    //             progress: undefined,
    //             theme: "light",
    //         });
    //     } else {
    //         setMaleExceed(false);
    //     }

    // },[westMaleCount,southMaleCount,proximityMaleCount,centralMaleCount])

    return (
        <div style={{borderLeft:"1px solid black "}} className='px-5'>
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
            />
            <h6>Capacity: {gyalpozhingCapacity?gyalpozhingCapacity:0}</h6>
            <form action="" className="mt-4">
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Total Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={maleCount}
                        onChange={handleMaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Total Female</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={femaleCount}
                        onChange={handleFemaleCountChange}
                        style={{border:`${maleExceed === true?'1px solid red':''}`}}
                    />
                </div>
                {/* <div className="d-flex flex-column">
                    <label htmlFor="maleCount">Proximity Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={proximityMaleCount}
                        onChange={handleProximityMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">South Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={southMaleCount}
                        onChange={handleSouthMaleChange}
                    />
                </div>
                <div className="d-flex flex-column">
                    <label htmlFor="maleCount">West Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="maleCount"
                        value={westMaleCount}
                        onChange={handleWestMaleChange}
                    />
                </div>
                <div className="d-flex flex-column mt-2">
                    <label htmlFor="femaleCount">Central Male</label>
                    <input
                        type="number"
                        className="form-control"
                        id="femaleCount"
                        value={centralMaleCount}
                        onChange={handleCentralMaleChange}
                    />
                </div> */}
            </form>
        </div>
    );
};

export default SettingGender;