import React from "react";
// import SettingProximityDiversity from "./SettingProximityDiversity";
// import SettingRangeProximity from "./SettingRangeProximity";
// import CustomPieChart from "./SettingDiversity";
// import SettingGender from "./SettingGender";
import SettingGender from "./SettingGender/SettingGender";
import SettingProximityDiversity from './SettingProximity/SettingProximityDiversity';
import SettingRangeProximity from './SettingProximityRange/SettingRangeProximity';
// import SettingDiversity from "./SettingDiversity/SettingDiversity";

const GeneralSetting=({settingGenderError})=>{
    return(
        <div className="p-3">
            <div className="">
                <div className="d-flex justify-content-between mt-5">
                    <SettingProximityDiversity/>
                    <SettingRangeProximity/>
                    <SettingGender settingGenderError = {settingGenderError} />
                </div>
            </div>
        </div>
    )
}
export default GeneralSetting;