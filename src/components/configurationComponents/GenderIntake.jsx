import React, { useState } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export default function GenderIntake() {
  const [femaleCount, setFemaleCount] = useState(null);
  const [maleCount, setMaleCount] = useState(null);

  const handleFemaleChange = (event) => {
    const value = parseInt(event.target.value);
    setFemaleCount(value >= 0 ? value : null);
  };

  const handleMaleChange = (event) => {
    const value = parseInt(event.target.value);
    setMaleCount(value >= 0 ? value : null);
  };

  return (
    <Box
      component="form"
      sx={{
        '& .MuiTextField-root': { m: 1, width: '25ch' },
        display: 'flex',
        flexDirection: 'column', // Display text fields in columns
        justifyContent: 'center',
        alignItems: 'center', // Center the items horizontally
        gap: '50px', // Increase the gap between text fields,
        marginTop: '40px',
      }}
      noValidate
      autoComplete="off"
    >
      <TextField
        label="Female"
        id="outlined-size-small"
        size="small"
      
        
        value={femaleCount}
        onChange={handleFemaleChange}
      />
      <TextField
        label="Male"
        id="outlined-size-normal"
        size="small"
        
        
        value={maleCount}
        onChange={handleMaleChange}
      />
    </Box>
  );
}
    