import * as React from 'react';
import { LineChart } from '@mui/x-charts/LineChart';
import Header from '../components/Header';

const years = [
  new Date(1990, 0, 1),
  new Date(1991, 0, 1),
  new Date(1992, 0, 1),
  new Date(1993, 0, 1),
  new Date(1994, 0, 1),
  new Date(1995, 0, 1),
  new Date(1996, 0, 1),
  new Date(1997, 0, 1),
  new Date(1998, 0, 1),
  new Date(1999, 0, 1),
  new Date(2000, 0, 1),
  new Date(2001, 0, 1),
  new Date(2002, 0, 1),
  new Date(2003, 0, 1),
  new Date(2004, 0, 1),
  new Date(2005, 0, 1),
  new Date(2006, 0, 1),
  new Date(2007, 0, 1),
  new Date(2008, 0, 1),
  new Date(2009, 0, 1),
  new Date(2010, 0, 1),
  new Date(2011, 0, 1),
  new Date(2012, 0, 1),
  new Date(2013, 0, 1),
  new Date(2014, 0, 1),
  new Date(2015, 0, 1),
  new Date(2016, 0, 1),
  new Date(2017, 0, 1),
  new Date(2018, 0, 1),
  new Date(2019, 0, 1),
];

const male= [
    7000, 6200, 5300, 5400, 5500, 5600, 5700, 5800, 5900,
  6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800,
  6900, 6950, 6925, 6900, 6875, 6850, 6825, 6800, 6775,
  6750, 6725, 6700,
];

const female = [
    6100, 5800, 6300, 6400, 6500, 6600, 6700, 6800, 6900,
    7000, 7100, 7200, 7300, 4000, 7500, 7600, 7700, 7800,
    7900, 7950, 7925, 7900, 7875, 7850, 7825, 7800, 7775,
    7750, 7725, 7700,
];

export default function Report() {
  return (
    <>
      <Header/>
      <div className='mt-4'>
        <h4>Yearly Gender Statistics</h4>
      </div>
      <LineChart
        xAxis={[
          {
            id: 'Years',
            data: years,
            scaleType: 'time',
            valueFormatter: (date) => date.getFullYear().toString(),
          },
        ]}
        series={[
          {
            id: 'Male',
            label: 'Male',
            data: male,
        
            area: false,
            showMark: false,
            color: '#7D7D7D', 
          },
          {
            id: 'Female',
            label: 'Female',
            data: female,
            
            area: false,
            showMark: false,
            color: '#2D9CDB',
          },
        ]}
        width={1000}
        height={400}
        margin={{ left: 70 }}
      />
    </>
  );
}
