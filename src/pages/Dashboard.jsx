import Header from '../components/Header'
import Enlisteedetail from '../components/Enlisteedetail';
import graduateCap from "../img/game-icons_graduate-cap.png";
import React, { useState } from "react";
import Select from "react-select";
import "../style/Dashboard.css";  
import AcademyComponent from '../components/dashboard/AcademyComponent';
import { useGetSelectedAllocationQuery,useGetMaleAndFemaleCountQuery } from '../store';
import { useEffect } from 'react';
import { setDashboardTotalEnlistee,setDashboardTotalFemale,setDashboardTotalMale,useFetchAllAdminQuery } from '../store';
import { useDispatch,useSelector } from 'react-redux';
import { ImDropbox } from "react-icons/im";
import Preview from '../components/allocationRun/Preview';

function Dashboard() {

  const dispatch = useDispatch();

  const {data,isLoading,isError,refetch} = useGetSelectedAllocationQuery();
  const {data:allAdmin,isLoading:adminLoading,isError:adminError,refetch:adminRefetch} = useFetchAllAdminQuery();
  const {data:genderCount,isLoading:genderCountLoading,isError:genderCountError,refetch:genderCountRefetch} = useGetMaleAndFemaleCountQuery();

  const dashboardData = useSelector((state) => state.dashboard);

  const [years,setYears] = useState([]);
  const [enlistmentYearValue, setEnlistmentYearValue] = useState(null);
  const [academyValue, setAcademyValue] = useState(null);
  const [allAdmins,setAllAdmins] = useState([]);

  const getGender = (students) => {
    const genderCounts = students.reduce((counts, row) => {
        const gender = row['gender']; 
        counts[gender] = (counts[gender] || 0) + 1;
        return counts;
      }, {});

      console.log('Gender Counts from dashboard:', genderCounts);
    return genderCounts;
}

  useEffect(() => {
    if (data && data?.allocation && data?.allocation[0]) {

      let totalStudent = 0;
      let totalMale = 0;
      let totalFemale = 0;
      
      data?.allocation[0]?.forEach((item) => {
        console.log("dashboar",item.numberOfMale)
        totalStudent += item.students.length;
        console.log("from dashboard student ",item);
        const genderCount = getGender(item.students);
        totalMale += genderCount.M;
        totalFemale += genderCount.F;
      })

      dispatch(setDashboardTotalEnlistee(totalStudent));
      dispatch(setDashboardTotalFemale(totalFemale))
      dispatch(setDashboardTotalMale(totalMale));

      const newAllocation = data?.allocation[0][0].year;
      setEnlistmentYearValue(newAllocation)

      console.log(data?.allocation);
      let uniqueYearsSet = new Set();
      data?.allocation.forEach(element => {
        const year = element[0].year;
        console.log(year);
        
        if (!uniqueYearsSet.has(year)) {
          uniqueYearsSet.add(year);
        }
      });

      const uniqueYearsArray = Array.from(uniqueYearsSet).sort((a, b) => a - b);;
      console.log(uniqueYearsArray);

      setYears(uniqueYearsArray)
    } else {
      dispatch(setDashboardTotalEnlistee(0));
      dispatch(setDashboardTotalFemale(0))
      dispatch(setDashboardTotalMale(0));
    }
  },[data]);

  useEffect(() => {
    console.log("admin from dashboard ",allAdmin);
    if (allAdmin?.users.length > 0) {
      setAllAdmins(allAdmin.users);
    }
  },[allAdmin])

  useEffect(() => {
    console.log("gender count ",genderCount);
    if (genderCount) {
      dispatch(setDashboardTotalEnlistee(genderCount?.female_count + genderCount?.male_count));
      dispatch(setDashboardTotalFemale(genderCount?.female_count))
      dispatch(setDashboardTotalMale(genderCount?.male_count));
    } else {
      dispatch(setDashboardTotalEnlistee(0));
      dispatch(setDashboardTotalFemale(0))
      dispatch(setDashboardTotalMale(0));
    }
  },[genderCount])

  console.log("from dashboard ",allAdmins);

  const [enlistmentOptions, setEnlistmentOptions] = useState([
    { value: "2000", label: "2000" },
    { value: "2001", label: "2001" }
  ]);

  useEffect(() => {
    if (years.length > 0) {
      const newEnlistmentOptions = years.map(year => ({ value: year.toString(), label: year.toString()}));
      setEnlistmentOptions(newEnlistmentOptions);
    }
  }, [years]);

  useEffect(() => {
    refetch();
  },[])

  const academyOptions = [
    { value: "Gyalpozhing Academy", label: "Gyalpozing" },
    { value: "Pemathang Academy", label: "Khotokha" },
    { value: "Jamtsholing Academy", label: "Pemathang" },
    { value: "Khotokha Academy", label: "Jamtsholing" },
  ];

  const formatDate = (dateString) => {
    const options = {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
    };
    return new Date(dateString).toLocaleString('en-US', options);
};

const getFirstLetter = (name) => {
  let u = "";
  const user = name.split(" ");
  user.forEach(element => {
    u+=element.charAt(0);
  });

  return u;
}

const getRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

  return (
    <div>
      <div className='top_headerenlistee_details_container'>
        <Header />
        <Enlisteedetail />
        <div style={{marginTop:'150px'}} >
          <Preview previewType = "dashboard"/>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;