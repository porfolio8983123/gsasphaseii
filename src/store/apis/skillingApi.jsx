import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const skillingApi = createApi({
    reducerPath: 'skilling',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchSkillings: builder.query({
                query: () => {
                    return {
                        url: '/skilling',
                        method: 'GET'
                    }
                }
            }),
            addSkilling: builder.mutation({
                query: ({data}) => ({
                    url: '/skilling',
                    method:'POST',
                    body:data
                })
            }),
            updateSkilling: builder.mutation({
                query: ({academySkillingId,data}) => ({
                    url: `/skilling/${academySkillingId}`,
                    method:'PUT',
                    body: data
                })
            }),
            deleteSkilling: builder.mutation({
                query: ({academySkillingId}) => ({
                    url: `/skilling/delete/${academySkillingId}`,
                    method: 'DELETE'
                })
            })
        }
    }
})

export const {useFetchSkillingsQuery, useAddSkillingMutation,useDeleteSkillingMutation,useUpdateSkillingMutation} = skillingApi;
export {skillingApi};