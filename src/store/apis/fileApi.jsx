import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const fileApi = createApi({
    reducerPath:'files',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchFiles: builder.query({
                query: () => {
                    return {
                        url:'/getAllCSVFile',
                        method:'GET'
                    }
                }
            }),
            deleteFile: builder.mutation({
                query: (fileId) => ({
                    url: `deleteFile/${fileId}`,
                    method: 'DELETE'
                })
            }),
            checkFileName: builder.mutation({
                query: (data) => ({
                    url: '/checkFileName',
                    method:'POST',
                    body:data
                })
            }),
            getDzongkhag: builder.query({
                query: () => {
                    return {
                        url:'/dzongkhag',
                        method:'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchFilesQuery,useDeleteFileMutation,useCheckFileNameMutation,useGetDzongkhagQuery} = fileApi;
export {fileApi};