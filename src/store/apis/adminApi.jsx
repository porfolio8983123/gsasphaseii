import { createApi,fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import Cookies from "js-cookie";

const adminApi = createApi({
    reducerPath: 'admin',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchAllAdmin: builder.query({
                query: () => {
                    return {
                        url:'/getAllUsers',
                        method:'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchAllAdminQuery} = adminApi; 
export {adminApi};