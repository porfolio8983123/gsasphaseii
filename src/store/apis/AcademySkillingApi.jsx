import {createApi,fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import Cookies from 'js-cookie';

const academySkillingApi = createApi({
    reducerPath: 'academyskilling',
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://localhost:8000/api',
        prepareHeaders(headers) {
            const token = Cookies.get('auth_token');
            headers.set('Authorization', `Bearer ${token}`);
            return headers;
        },
    }),
    endpoints(builder) {
        return {
            fetchAcademySkilling: builder.query({
                query: () => {
                    return {
                        url: '/getSkilling',
                        method: 'GET'
                    }
                }
            })
        }
    }
})

export const {useFetchAcademySkillingQuery} = academySkillingApi;
export {academySkillingApi};