import { createSlice } from "@reduxjs/toolkit";

const sideLineSlice = createSlice({
    name:'sideLine',
    initialState: {
        value:0
    },
    reducers: {
        updateSideLine: (state,action) => {
            state.value = action.payload;
        }
    }
})

export const {updateSideLine} = sideLineSlice.actions;
export const sideLineReducer = sideLineSlice.reducer;