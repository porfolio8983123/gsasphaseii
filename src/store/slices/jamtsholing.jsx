import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    academy:'Jamtsholing Academy',
    proximity: 2100,
    proximityRange:5,
    east:300,
    central:300,
    south:300,
    west:0,
    numberOfFemale:2750,
    numberOfMale:2750,
    southMale:300,
    westMale:700,
    centralMale:200,
    eastMale:300
}

const jamtsholingSlice = createSlice({
    name:'jamtsholing',
    initialState,
    reducers: {
        setJamtsholingAcademy: (state,action) => {
            state.academy = action.payload;
        },
        setJamtsholingProximity: (state,action) => {
            state.proximity = action.payload;
        },
        setJamtsholingProximityRange: (state,action) => {
            state.proximityRange = action.payload;
        },
        setJamtsholingCentral: (state, action) => {
            state.central = action.payload;
        },
        setJamtsholingSouth: (state, action) => {
        state.south = action.payload;
        },
        setJamtsholingEast: (state, action) => {
        state.east = action.payload;
        },
        setJamtsholingNumberOfFemale: (state, action) => {
        state.numberOfFemale = action.payload;
        },
        setJamtsholingNumberOfMale: (state, action) => {
        state.numberOfMale = action.payload;
        },
        setJamtsholingSouthMale: (state,action) => {
            state.southMale = action.payload;
        },
        setJamtsholingEastMale:(state,action) => {
            state.eastMale = action.payload;
        },
        setJamtsholingCentralMale: (state,action) => {
            state.centralMale = action.payload;
        },
        setJamtsholingProximityMale: (state,action) => {
            state.westMale = action.payload;
        }
    }
})

export const {setJamtsholingProximity,
    setJamtsholingCentral,
    setJamtsholingEast,
    setJamtsholingNumberOfFemale,
    setJamtsholingNumberOfMale,setJamtsholingProximityRange,
    setJamtsholingSouth,
    setJamtsholingCentralMale,setJamtsholingProximityMale,
    setJamtsholingSouthMale,
    setJamtsholingEastMale
    } = jamtsholingSlice.actions;

export const jamtsholingReducer = jamtsholingSlice.reducer;