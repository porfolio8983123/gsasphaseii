import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    academy:'Gyalpozhing Academy',
    proximity: 889,
    proximityRange:9,
    east:0,
    central:265,
    south:248,
    west:1598,
    numberOfFemale:500,
    numberOfMale:500,
    southMale:150,
    westMale:800,
    centralMale:250,
    eastMale:300
}

const gyalpozhingSlice = createSlice({
    name:'gyalpozhing',
    initialState,
    reducers: {
        setAcademy: (state,action) => {
            state.academy = action.payload;
        },
        setProximity: (state,action) => {
            state.proximity = action.payload;
        },
        setProximityRange: (state,action) => {
            state.proximityRange = action.payload;
        },
        setCentral: (state, action) => {
            state.central = action.payload;
        },
        setSouth: (state, action) => {
        state.south = action.payload;
        },
        setWest: (state, action) => {
        state.west = action.payload;
        },
        setNumberOfFemale: (state, action) => {
        state.numberOfFemale = action.payload;
        },
        setNumberOfMale: (state, action) => {
        state.numberOfMale = action.payload;
        },
        setSouthMale: (state,action) => {
            state.southMale = action.payload;
        },
        setWestMale:(state,action) => {
            state.westMale = action.payload;
        },
        setCentralMale: (state,action) => {
            state.centralMale = action.payload;
        },
        setProximityMale: (state,action) => {
            state.eastMale = action.payload;
        }
    }
})

export const {setAcademy,
    setProximity,
    setProximityRange,
    setCentral,
    setSouth,
    setWest,
    setNumberOfFemale,
    setNumberOfMale,
    setCentralMale,
    setProximityMale,
    setSouthMale,
    setWestMale
} = gyalpozhingSlice.actions;
export const gyalpozhingReducer = gyalpozhingSlice.reducer;