import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    academy:'Pemathang Academy',
    proximity: 943,
    proximityRange:9,
    east:0,
    central:463,
    south:506,
    west:1088,
    numberOfFemale:2250,
    numberOfMale:2250,
    southMale:200,
    westMale:700,
    centralMale:200,
    eastMale:400
}

const pemathangSlice = createSlice({
    name:'pemathang',
    initialState,
    reducers: {
        setPemathangAcademy: (state,action) => {
            state.academy = action.payload;
        },
        setPemathangProximity: (state,action) => {
            state.proximity = action.payload;
        },
        setPemathangProximityRange: (state,action) => {
            state.proximityRange = action.payload;
        },
        setPemathangCentral: (state, action) => {
            state.central = action.payload;
        },
        setPemathangSouth: (state, action) => {
        state.south = action.payload;
        },
        setPemathangWest: (state, action) => {
        state.west = action.payload;
        },
        setPemathangNumberOfFemale: (state, action) => {
        state.numberOfFemale = action.payload;
        },
        setPemathangNumberOfMale: (state, action) => {
        state.numberOfMale = action.payload;
        },
        setPemathangSouthMale: (state,action) => {
            state.southMale = action.payload;
        },
        setPemathangWestMale:(state,action) => {
            state.westMale = action.payload;
        },
        setPemathangCentralMale: (state,action) => {
            state.centralMale = action.payload;
        },
        setPemathangProximityMale: (state,action) => {
            state.eastMale = action.payload;
        }
    }
})

export const {setAcademy,
    setPemathangProximity,
    setPemathangCentral,
    setPemathangNumberOfFemale,
    setPemathangNumberOfMale,
    setPemathangProximityRange,
    setPemathangSouth,
    setPemathangWest,
    setPemathangCentralMale,
    setPemathangProximityMale,
    setPemathangSouthMale,
    setPemathangWestMale
} = pemathangSlice.actions;

export const pemathangReducer = pemathangSlice.reducer;