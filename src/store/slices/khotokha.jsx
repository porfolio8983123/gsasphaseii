import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    academy:'Khotokha Academy',
    proximity: 966,
    proximityRange:9,
    east:616,
    central:0,
    south:629,
    west:523,
    numberOfFemale:1000,
    numberOfMale:1000,
    southMale:136,
    westMale:190,
    centralMale:229,
    eastMale:236
}

const khotokhaSlice = createSlice({
    name:'khotokha',
    initialState,
    reducers: {
        setKhotokhaAcademy: (state,action) => {
            state.academy = action.payload;
        },
        setKhotokhaProximity: (state,action) => {
            state.proximity = action.payload;
        },
        setKhotokhaProximityRange: (state,action) => {
            state.proximityRange = action.payload;
        },
        setKhotokhaWest: (state, action) => {
            state.west = action.payload;
        },
        setKhotokhaSouth: (state, action) => {
        state.south = action.payload;
        },
        setKhotokhaEast: (state, action) => {
        state.east = action.payload;
        },
        setKhotokhaNumberOfFemale: (state, action) => {
        state.numberOfFemale = action.payload;
        },
        setKhotokhaNumberOfMale: (state, action) => {
        state.numberOfMale = action.payload;
        },
        setKhotokhaSouthMale: (state,action) => {
            state.southMale = action.payload;
        },
        setKhotokhaWestMale:(state,action) => {
            state.westMale = action.payload;
        },
        setKhotokhaEastMale: (state,action) => {
            state.eastMale = action.payload;
        },
        setKhotokhaProximityMale: (state,action) => {
            state.centralMale = action.payload;
        }
    }
})

export const {setKhotokhaProximity,
    setKhotokhaNumberOfFemale,
    setKhotokhaNumberOfMale,
    setKhotokhaProximityRange,
    setKhotokhaSouth,
    setKhotokhaWest,
    setKhotokhaEast,
    setKhotokhaEastMale,
    setKhotokhaProximityMale,
    setKhotokhaSouthMale,
    setKhotokhaWestMale
    } = khotokhaSlice.actions;

export const khotokhaReducer = khotokhaSlice.reducer;