import { createSlice } from "@reduxjs/toolkit";

const dashboardSlicde = createSlice({
    name:'dashboard',
    initialState: {
        totalAcademy:0,
        totalEnlistee:0,
        totalMale:0,
        totalFemale:0
    },
    reducers: {
        setDashboardTotalAcademy: (state,action) => {
            state.totalAcademy = action.payload;
        },
        setDashboardTotalEnlistee: (state,action) => {
            state.totalEnlistee = action.payload;
        },
        setDashboardTotalMale:(state,action) => {
            state.totalMale = action.payload;
        },
        setDashboardTotalFemale: (state,action) => {
            state.totalFemale = action.payload;
        }
    }
})

export const {setDashboardTotalAcademy,setDashboardTotalEnlistee,setDashboardTotalFemale,setDashboardTotalMale} = dashboardSlicde.actions;
export const dashboardReducer = dashboardSlicde.reducer;