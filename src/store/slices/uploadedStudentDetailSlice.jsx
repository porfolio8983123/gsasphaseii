import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    totalMale:0,
    totalFemale:0,
    totalEast:0,
    totalWest:0,
    totalCentral:0,
    totalSouth:0,
    totalMaleEast: 0,
    totalMaleWest: 0,
    totalMaleCentral: 0,
    totalMaleSouth: 0,
    totalFemaleEast: 0,
    totalFemaleWest: 0,
    totalFemaleCentral: 0,
    totalFemaleSouth: 0,
}

const uploadedStudentDetailSlice = createSlice({
    name:'studentCount',
    initialState,
    reducers: {
        setTotalMale: (state,action) => {
            state.totalMale = action.payload;
        },
        setTotalFemale: (state,action) => {
            state.totalFemale = action.payload;
        },
        setTotalEast: (state,action) => {
            state.totalEast = action.payload;
        },
        setTotalWest: (state, action) => {
            state.totalWest = action.payload;
        },
        setTotalSouth: (state, action) => {
            state.totalSouth = action.payload;
        },
        setTotalCentral: (state, action) => {
            state.totalCentral = action.payload;
        },
        setTotalMaleEast: (state,action) => {
            state.totalMaleEast = action.payload;
        },
        setTotalMaleWest: (state,action) => {
            state.totalMaleWest = action.payload;
        },
        setTotalMaleSouth: (state,action) => {
            state.totalMaleSouth = action.payload;
        },
        setTotalMaleCentral: (state,action) => {
            state.totalMaleCentral = action.payload;
        },
        setTotalFemaleEast: (state,action) => {
            state.totalFemaleEast = action.payload;
        },
        setTotalFemaleWest: (state,action) => {
            state.totalFemaleWest = action.payload;
        },
        setTotalFemaleSouth: (state,action) => {
            state.totalFemaleSouth = action.payload;
        },
        setTotalFemaleCentral: (state,action) => {
            state.totalFemaleCentral = action.payload;
        }
    }
})

export const {
    setTotalCentral,
    setTotalEast,
    setTotalFemale,
    setTotalMale,
    setTotalSouth,
    setTotalWest,
    setTotalFemaleCentral,
    setTotalFemaleEast,
    setTotalFemaleSouth,
    setTotalFemaleWest,
    setTotalMaleCentral,
    setTotalMaleEast,
    setTotalMaleSouth,
    setTotalMaleWest
} = uploadedStudentDetailSlice.actions;

export const uploadedStudentDetailReducer = uploadedStudentDetailSlice.reducer;