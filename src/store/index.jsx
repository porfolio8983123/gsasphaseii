import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { fileApi } from "./apis/fileApi";
import {academyApi} from './apis/academyApi';
import { setupListeners } from "@reduxjs/toolkit/query";
import {profileApi} from './apis/profileApi';
import { fileReducer,updateData,addStudentData } from "./slices/fileSlices";
import {updateSideLine, sideLineReducer} from './slices/sideLineSlice';
import { gyalpozhingReducer,setAcademy,setCentral,setNumberOfFemale,setNumberOfMale,setProximity,setProximityRange,setSouth,setWest,setCentralMale,setProximityMale,setSouthMale,setWestMale } from "./slices/gyalpozhing";
import { allocationApi } from "./apis/allocationApi";
import { enlisteeApi } from "./apis/enlisteeApi";
import { adminApi } from "./apis/adminApi";
import { pemathangReducer,setPemathangCentral,setPemathangNumberOfFemale,setPemathangNumberOfMale,setPemathangProximity,setPemathangProximityRange,setPemathangSouth,setPemathangWest,setPemathangCentralMale,setPemathangProximityMale,setPemathangSouthMale,setPemathangWestMale} from "./slices/pemathang";
import { jamtsholingReducer,setJamtsholingCentral,setJamtsholingEast,setJamtsholingNumberOfFemale,setJamtsholingNumberOfMale,setJamtsholingProximity,setJamtsholingProximityRange,setJamtsholingSouth,setJamtsholingCentralMale,setJamtsholingProximityMale,setJamtsholingSouthMale,setJamtsholingEastMale } from "./slices/jamtsholing";
import { khotokhaReducer,setKhotokhaEast,setKhotokhaNumberOfFemale,setKhotokhaNumberOfMale,setKhotokhaProximity,setKhotokhaProximityRange,setKhotokhaSouth,setKhotokhaWest,setKhotokhaEastMale,setKhotokhaProximityMale,setKhotokhaSouthMale,setKhotokhaWestMale } from "./slices/khotokha";
import { uploadedStudentDetailReducer,setTotalCentral,setTotalEast,setTotalFemale,setTotalMale,setTotalSouth,setTotalWest,setTotalFemaleCentral,setTotalFemaleEast,setTotalFemaleSouth,setTotalFemaleWest,setTotalMaleCentral,setTotalMaleEast,setTotalMaleSouth,setTotalMaleWest } from "./slices/uploadedStudentDetailSlice";
import { setDashboardTotalAcademy,setDashboardTotalEnlistee,setDashboardTotalFemale,setDashboardTotalMale,dashboardReducer } from "./slices/dashboardSlice";
import { skillingApi } from "./apis/skillingApi";
import { academySkillingApi } from "./apis/AcademySkillingApi";

export const store = configureStore({
    reducer: {
        [fileApi.reducerPath]:fileApi.reducer,
        [academyApi.reducerPath]:academyApi.reducer,
        [profileApi.reducerPath]:profileApi.reducer,
        [allocationApi.reducerPath]:allocationApi.reducer,
        [enlisteeApi.reducerPath]:enlisteeApi.reducer,
        [adminApi.reducerPath]:adminApi.reducer,
        [skillingApi.reducerPath]:skillingApi.reducer,
        [academySkillingApi.reducerPath]:academySkillingApi.reducer,
        file: fileReducer,
        gyalpozhing: gyalpozhingReducer,
        pemathang:pemathangReducer,
        jamtsholing:jamtsholingReducer,
        khotokha:khotokhaReducer,
        studentCount:uploadedStudentDetailReducer,
        dashboard:dashboardReducer,
        sideLine:sideLineReducer,
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware()
            .concat(fileApi.middleware)
            .concat(academyApi.middleware)
            .concat(profileApi.middleware)
            .concat(allocationApi.middleware)
            .concat(enlisteeApi.middleware)
            .concat(adminApi.middleware)
            .concat(skillingApi.middleware)
            .concat(academySkillingApi.middleware);
    }
})

setupListeners(store.dispatch);

export {useFetchFilesQuery,useDeleteFileMutation,useCheckFileNameMutation,useGetDzongkhagQuery} from './apis/fileApi';
export {useFetchAcademiesQuery,useAddAcademyMutation, useUpdateAcademyMutation,useUpdateAcademyStatusMutation} from './apis/academyApi';
export {useStartAllocationMutation,useFetchhAllAllocatedQuery,useFetchAllocationQuery,useFetchAllocatedQuery,useDeleteAllocationMutation,useUpdateAllocationMutation,useGetSelectedAllocationQuery,useGetAllocationByAdminQuery,useGetMaleAndFemaleCountQuery} from './apis/allocationApi';
export {useAddStudentMutation} from './apis/enlisteeApi';
export {useFetchAdminQuery,useChangePasswordMutation} from './apis/profileApi';
export {useFetchAllAdminQuery} from './apis/adminApi';
export {useFetchSkillingsQuery,useAddSkillingMutation,useDeleteSkillingMutation,useUpdateSkillingMutation} from './apis/skillingApi';
export {useFetchAcademySkillingQuery} from './apis/AcademySkillingApi';

export {updateData,setAcademy,setCentral,setNumberOfFemale,setNumberOfMale,setProximity,setProximityRange,setSouth,setWest,addStudentData,
    setPemathangCentral,
    setPemathangNumberOfFemale,
    setPemathangNumberOfMale,
    setPemathangProximity,
    setPemathangProximityRange,
    setPemathangSouth,
    setPemathangWest,
    setJamtsholingCentral,
    setJamtsholingEast,
    setJamtsholingNumberOfFemale,
    setJamtsholingNumberOfMale,
    setJamtsholingProximity,
    setJamtsholingProximityRange,
    setJamtsholingSouth,
    setKhotokhaEast,
    setKhotokhaNumberOfFemale,
    setKhotokhaNumberOfMale,
    setKhotokhaProximity,
    setKhotokhaProximityRange,
    setKhotokhaSouth,
    setKhotokhaWest,
    setTotalCentral,
    setTotalEast,
    setTotalFemale,
    setTotalMale,
    setTotalSouth,
    setTotalWest,
    setCentralMale,
    setProximityMale,
    setSouthMale,
    setWestMale,
    setTotalFemaleCentral,
    setTotalFemaleEast,
    setTotalFemaleSouth,
    setTotalFemaleWest,
    setTotalMaleCentral,
    setTotalMaleEast,
    setTotalMaleSouth,
    setTotalMaleWest,
    setJamtsholingCentralMale,
    setJamtsholingProximityMale,
    setJamtsholingSouthMale,
    setJamtsholingEastMale,
    setKhotokhaEastMale,
    setKhotokhaProximityMale,
    setKhotokhaSouthMale,
    setKhotokhaWestMale,
    setPemathangCentralMale,
    setPemathangProximityMale,
    setPemathangSouthMale,
    setPemathangWestMale,
    setDashboardTotalAcademy,
    setDashboardTotalEnlistee,
    setDashboardTotalFemale,
    setDashboardTotalMale,
    updateSideLine
};